#!/bin/bash
RUNS="0..4"
if [ "$2" != "" ]; then
    RUNS="$2"
fi

opp_runall -j4 ../../src/plasasim brownout-epa.ini -u Cmdenv -c $1 -n ..:../../src:../../../queueinglib -l ../../../queueinglib/queueinglib -r $RUNS
pushd ../../../../results/BrownoutEPA
scavetool index $1*.vec
popd

#paplay /usr/share/sounds/gnome/default/alerts/bark.ogg
paplay /usr/share/sounds/freedesktop/stereo/dialog-information.oga
