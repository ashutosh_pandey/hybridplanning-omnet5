/**
 * Copyright (c) 2015 Carnegie Mellon University. All Rights Reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:

 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following acknowledgments
 * and disclaimers.

 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.

 * 3. The names "Carnegie Mellon University," "SEI" and/or "Software
 * Engineering Institute" shall not be used to endorse or promote
 * products derived from this software without prior written
 * permission. For written permission, please contact
 * permission@sei.cmu.edu.

 * 4. Products derived from this software may not be called "SEI" nor
 * may "SEI" appear in their names without prior written permission of
 * permission@sei.cmu.edu.

 * 5. Redistributions of any form whatsoever must retain the following
 * acknowledgment:

 * This material is based upon work funded and supported by the
 * Department of Defense under Contract No. FA8721-05-C-0003 with
 * Carnegie Mellon University for the operation of the Software
 * Engineering Institute, a federally funded research and development
 * center.

 * Any opinions, findings and conclusions or recommendations expressed
 * in this material are those of the author(s) and do not necessarily
 * reflect the views of the United States Department of Defense.

 * NO WARRANTY. THIS CARNEGIE MELLON UNIVERSITY AND SOFTWARE
 * ENGINEERING INSTITUTE MATERIAL IS FURNISHED ON AN "AS-IS"
 * BASIS. CARNEGIE MELLON UNIVERSITY MAKES NO WARRANTIES OF ANY KIND,
 * EITHER EXPRESSED OR IMPLIED, AS TO ANY MATTER INCLUDING, BUT NOT
 * LIMITED TO, WARRANTY OF FITNESS FOR PURPOSE OR MERCHANTABILITY,
 * EXCLUSIVITY, OR RESULTS OBTAINED FROM USE OF THE MATERIAL. CARNEGIE
 * MELLON UNIVERSITY DOES NOT MAKE ANY WARRANTY OF ANY KIND WITH
 * RESPECT TO FREEDOM FROM PATENT, TRADEMARK, OR COPYRIGHT
 * INFRINGEMENT.

 * This material has been approved for public release and unlimited
 * distribution.

 * DM-0002494
**/
/*
 * HPAdaptationManager.cpp
 *
 *  Created on: Apr 12, 2015
 *      Author: ashutosh
 */

#include <HPAdaptationManager.h>
#include <Plan/ReactivePlanner.h>
#include <Plan/PlanDB.h>
#include "UtilityScorer.h"
#include <iostream>
#include <sstream>
#include "AdaptationPlanner.h"
#include <fstream>
#include "PredictableRandomSource.h"
#include <iomanip>
#include <stack>
#include <string>
#include <boost/tokenizer.hpp>
#include <cstdlib>
#include <set>
#include <map>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include "ModulePriorities.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <EnvPredictionRecord.h>
#include <MGCQueue.h>
#include "Verification.h"
#include <Utils.h>
#include <TimeSeriesSimilarity.h>
#include <ClassifyProblemInstance.h>
#include <DebugFileInfo.h>

using namespace std;
Define_Module(HPAdaptationManager);

HPAdaptationManager::HPAdaptationManager() {
    // TODO Auto-generated constructor stub
    usePredictor =
            omnetpp::getSimulation()->getSystemModule()->par("usePredictor").boolValue();
}

HPAdaptationManager::~HPAdaptationManager() {
    // TODO Auto-generated destructor stub
}

string get_env_value(int evaluation_period) {
    string env = "(s = ";
    env += std::to_string(evaluation_period);
    env += " ? ";

    switch (evaluation_period) {
    case 1:
        env += "0.0185442";
        break;
    case 2:
        env += "0.0185028";
        break;
    case 3:
        env += "0.0199687";
        break;
    case 4:
        env += "0.0200779";
        break;
    case 5:
        env += "0.0183286";
        break;
    case 6:
        env += "0.0185966";
        break;
    case 7:
        env += "0.0158014";
        break;
    case 8:
        env += "0.0185256";
        break;
    case 9:
        env += "0.0172753";
        break;
    case 10:
        env += "0.0149158";
        break;
    case 11:
        env += "0.0156696";
        break;
    case 12:
        env += "0.0149646";
        break;
    case 13:
        env += "0.0129409";
        break;
    case 14:
        env += "0.0143129";
        break;
    case 15:
        env += "0.012908";
        break;
    case 16:
        env += "0.0126704";
        break;
    case 17:
        env += "0.0136644";
        break;
    case 18:
        env += "0.0133373";
        break;
    case 19:
        env += "0.0119273";
        break;
    case 20:
        env += "0.0090703";
        break;
    case 21:
        env += "0.00879577";
        break;
    case 22:
        env += "0.00854561";
        break;
    case 23:
        env += "0.00926523";
        break;
    case 24:
        env += "0.00877777";
        break;
    case 25:
        env += "0.00871189";
        break;
    case 26:
        env += "0.00804859";
        break;
    case 27:
        env += "0.00780405";
        break;
    case 28:
        env += "0.00792207";
        break;
    case 29:
        env += "0.00795251";
        break;
    case 30:
        env += "0.00749685";
        break;
    case 31:
        env += "0.00674501";
        break;
    case 32:
        env += "0.00756305";
        break;
    case 33:
        env += "0.00893382";
        break;
    case 34:
        env += "0.00998392";
        break;
    case 35:
        env += "0.0100628";
        break;
    case 36:
        env += "0.0100136";
        break;
    case 37:
        env += "0.0103759";
        break;
    case 38:
        env += "0.0110962";
        break;
    case 39:
        env += "0.0111993";
        break;
    case 40:
        env += "0.0123696";
        break;
    case 41:
        env += "0.0121001";
        break;
    case 42:
        env += "0.0112575";
        break;
    case 43:
        env += "0.0103757";
        break;
    case 44:
        env += "0.00945316";
        break;
    case 45:
        env += "0.0087403";
        break;
    case 46:
        env += "0.00876926";
        break;
    case 47:
        env += "0.00856684";
        break;
    case 48:
        env += "0.00850081";
        break;
    case 49:
        env += "0.00866338";
        break;
    case 50:
        env += "0.00854196";
        break;
    case 51:
        env += "0.0079456";
        break;
    case 52:
        env += "0.00756412";
        break;
    case 53:
        env += "0.00667872";
        break;
    case 54:
        env += "0.00808097";
        break;
    case 55:
        env += "0.00966343";
        break;
    case 56:
        env += "0.0106719";
        break;
    case 57:
        env += "0.0115544";
        break;
    case 58:
        env += "0.0114222";
        break;
        /*case 59:
         env += "0.00945316";
         break;
         case 60:
         env += "0.0087403";
         break;
         case 61:
         env += "0.00876926";
         break;
         case 62:
         env += "0.00856684";
         break;
         case 63:
         env += "0.00850081";
         break;
         case 64:
         env += "0.00866338";
         break;
         case 65:
         env += "0.00854196";
         break;
         case 66:
         env += "0.0079456";
         break;
         case 67:
         env += "0.00756412";
         break;
         case 68:
         env += "0.00667872";
         break;
         case 69:
         env += "0.00808097";
         break;
         case 70:
         env += "0.00966343";
         break;
         case 71:
         env += "0.0106719";
         break;
         case 72:
         env += "0.0115544";
         break;
         case 73:
         env += "0.0114222";
         break;
         case 74:
         env += "0.0121236";
         break;*/
    default:
        assert(false);
    }

    env += " : 0)";

    return env;
}

void dump_arrival_rates(const double & arrival_rate, HPModel* hpModel) {
    string file_name = DebugFileInfo::getInstance()->GetDebugFilePath();
            //"/home/ashutosp/Dropbox/regression/HP_triggers_arrival_rate";
    std::ofstream myfile;
    //hpModel->getObservations().avgResponseTime;
    myfile.open(file_name, ios::app);
    myfile << arrival_rate << "====" << 1 / arrival_rate << "===="
            << hpModel->getObservations().avgResponseTime << "\n";
    myfile.close();
}

void dump_str(const char* str) {
    string file_name = DebugFileInfo::getInstance()->GetDebugFilePath();
     //       "/home/ashutosp/Dropbox/regression/HP_triggers_arrival_rate";
    std::ofstream myfile;

    myfile.open(file_name, ios::app);
    myfile << str << "\n";
    myfile.close();
}

void dump_response_time(const double & rt) {
    string file_name = DebugFileInfo::getInstance()->GetResponseTimeFilePath();
        //    "/home/ashutosp/Dropbox/regression/HP_cycle_response_time";

    std::ofstream myfile;

    myfile.open(file_name, ios::app);
    myfile << rt << "\n";
    myfile.close();
}

void HPAdaptationManager::get_initial_state_str(stringstream& initialState,
        bool fastPlanning) {
    initialState << "const double addServer_LATENCY = "
            << hpModel->getBootDelay() << ";" << endl;
    initialState << "const int HORIZON = " << hpModel->getHorizon() << ";"
            << endl;
    initialState << "const double PERIOD = " << hpModel->getEvaluationPeriod()
            << ";" << endl;

    initialState << "const int DIMMER_LEVELS = "
            << hpModel->getNumberOfBrownoutLevels() << ";" << endl;
    initialState << "const int ini_dimmer = ";
    int discretizedBrownoutFactor = 1
            + (hpModel->getNumberOfBrownoutLevels() - 1)
                    * hpModel->getConfiguration().getBrownOutFactor();
    initialState << discretizedBrownoutFactor;
    initialState << ";" << endl;

    initialState << "const int MAX_SERVERS_A = "
            << hpModel->getMaxServers(MTServerAdvance::ServerType::A) << ";"
            << endl;
    initialState << "const int MAX_SERVERS_B = "
            << hpModel->getMaxServers(MTServerAdvance::ServerType::B) << ";"
            << endl;
    initialState << "const int MAX_SERVERS_C = "
            << hpModel->getMaxServers(MTServerAdvance::ServerType::C) << ";"
            << endl;

    initialState << "const int ini_servers_A = ";
    initialState
            << hpModel->getConfiguration().getActiveServers(
                    MTServerAdvance::ServerType::A) << ";" << endl;
    initialState << "const int ini_servers_B = ";
    initialState
            << hpModel->getConfiguration().getActiveServers(
                    MTServerAdvance::ServerType::B) << ";" << endl;
    initialState << "const int ini_servers_C = ";
    initialState
            << hpModel->getConfiguration().getActiveServers(
                    MTServerAdvance::ServerType::C) << ";" << endl;

    int addServerAState = 0;
    int addServerBState = 0;
    int addServerCState = 0;

    int addServerState = 0;

    if (hpModel->getConfiguration().getBootRemain() > 0) {
        int bootPeriods = hpModel->getBootDelay()
                / hpModel->getEvaluationPeriod();
        addServerState = min(
                bootPeriods - hpModel->getConfiguration().getBootRemain(),
                bootPeriods);
        MTServerAdvance::ServerType bootType =
                hpModel->getConfiguration().getBootType();

        switch (bootType) {
        case MTServerAdvance::ServerType::A:
            addServerAState = addServerState;
            break;
        case MTServerAdvance::ServerType::B:
            addServerBState = addServerState;
            break;
        case MTServerAdvance::ServerType::C:
            addServerCState = addServerState;
            break;
        case MTServerAdvance::ServerType::NONE:
            assert(false);
        }
    }

    initialState << "const int ini_addServerA_state = ";
    initialState << addServerAState;
    initialState << ";" << endl;

    initialState << "const int ini_addServerB_state = ";
    initialState << addServerBState;
    initialState << ";" << endl;

    initialState << "const int ini_addServerC_state = ";
    initialState << addServerCState;
    initialState << ";" << endl;

    initialState << "const double SERVERA_COST_SEC = "
            << omnetpp::getSimulation()->getSystemModule()->par("serverA_cost_sec").doubleValue()
            << ";" << endl;
    initialState << "const double SERVERB_COST_SEC = "
            << omnetpp::getSimulation()->getSystemModule()->par("serverB_cost_sec").doubleValue()
            << ";" << endl;
    initialState << "const double SERVERC_COST_SEC = "
            << omnetpp::getSimulation()->getSystemModule()->par("serverC_cost_sec").doubleValue()
            << ";" << endl;

    initialState << "const double MAX_ARRIVALA_CAPACITY = "
            << omnetpp::getSimulation()->getSystemModule()->par("max_arrival_capacity_A").doubleValue()
            << ";" << endl;
    initialState << "const double MAX_ARRIVALA_CAPACITY_LOW = "
            << omnetpp::getSimulation()->getSystemModule()->par("max_arrival_capacity_low_A").doubleValue()
            << ";" << endl;
    initialState << "const double MAX_ARRIVALB_CAPACITY = "
            << omnetpp::getSimulation()->getSystemModule()->par("max_arrival_capacity_B").doubleValue()
            << ";" << endl;
    initialState << "const double MAX_ARRIVALB_CAPACITY_LOW = "
            << omnetpp::getSimulation()->getSystemModule()->par("max_arrival_capacity_low_B").doubleValue()
            << ";" << endl;
    initialState << "const double MAX_ARRIVALC_CAPACITY = "
            << omnetpp::getSimulation()->getSystemModule()->par("max_arrival_capacity_C").doubleValue()
            << ";" << endl;
    initialState << "const double MAX_ARRIVALC_CAPACITY_LOW = "
            << omnetpp::getSimulation()->getSystemModule()->par("max_arrival_capacity_low_C").doubleValue()
            << ";" << endl;

    initialState << "const double penalty = "
            << omnetpp::getSimulation()->getSystemModule()->par("penalty").doubleValue() << ";"
            << endl;

    initialState << "const int ini_traffic_A = "
            << hpModel->getConfiguration().getTraffic(
                    MTServerAdvance::ServerType::A) << ";" << endl;
    initialState << "const int ini_traffic_B = "
            << hpModel->getConfiguration().getTraffic(
                    MTServerAdvance::ServerType::B) << ";" << endl;
    initialState << "const int ini_traffic_C = "
            << hpModel->getConfiguration().getTraffic(
                    MTServerAdvance::ServerType::C) << ";" << endl;

    if ((fastPlanning
            && omnetpp::getSimulation()->getSystemModule()->par(
                    "useInterArrivalScaleFactorForFastPlanning").boolValue())
            || (!fastPlanning && omnetpp::getSimulation()->getSystemModule()->par(
                    "useInterArrivalScaleFactorForSlowPlanning").boolValue())) {
        initialState << "const double interArrivalScaleFactorForDecision = "
                << par("interArrivalScaleFactorForDecision").doubleValue()
                << ";" << endl;
    } else {
        initialState << "const double interArrivalScaleFactorForDecision = "
                << "1" << ";" << endl;
    }
}

int frequency_of_primes(int n) {
    int i, j;
    int freq = n - 1;
    for (i = 2; i <= n; ++i)
        for (j = sqrt(i); j > 1; --j)
            if (i % j == 0) {
                --freq;
                break;
            }
    return freq;
}

void touch_util() {
    static bool a = true;

    if (a) {
        string file_name = DebugFileInfo::getInstance()->GetUtilityFilePath();
             //   "/home/ashutosp/Dropbox/regression/HPMonitor_utility";
        std::ofstream myfile;

        myfile.open(file_name, ios::app);
        myfile << 0000000 << "\n";
        myfile.close();
        a = false;
    }
}

void touch_rt() {
    static bool a = true;

    if (a) {
        string file_name = DebugFileInfo::getInstance()->GetResponseTimeFilePath();
             //   "/home/ashutosp/Dropbox/regression/HPModel_response_time";
        std::ofstream myfile;

        myfile.open(file_name, ios::app);
        myfile << 0000000 << "\n";
        myfile.close();
        a = false;
    }
}

void dump_predictions(double value, int horizon) {
    string file_name =
            "/home/ashutosp/testCode/plasacode/plasasim/simulations/brownout/PredictorTesting/";

    switch (horizon) {
    case 0:
        file_name += "horizon0";
        break;
    case 1:
        file_name += "horizon1";
        break;
    case 2:
        file_name += "horizon2";
        break;
    case 3:
        file_name += "horizon3";
        break;
    case 4:
        file_name += "horizon4";
        break;
    default:
        assert(false);
    }

    std::ofstream myfile;

    myfile.open(file_name, ios::app);
    myfile << double(1.0 / value) << "\n";
    myfile.close();
}

// these constants depend on the PRISM model
const string STATE_VAR = "s";
const string GUARD = "[tick] ";

// This function is hardcoded as it just looks at node 2.
double HPAdaptationManager::getNextEnvironmentPrediction() {
    double env = 0;
    ScenarioTree* pTree = hpModel->getEnvironmentProbabilityTree();
    ScenarioTree tree = *pTree;

    stack<ScenarioTree::NodeStateIndex> traversalStack;
    int stateIndex = 0;

    traversalStack.push(
            ScenarioTree::NodeStateIndex(&tree.getRoot(), stateIndex++));

    while (!traversalStack.empty()) {
        const ScenarioTree::Node* pNode = traversalStack.top().pNode;
        int nodeStateIndex = traversalStack.top().stateIndex;
        traversalStack.pop();

        if (nodeStateIndex == 2) {
            env = pNode->value;
            break;
        }

        if (!pNode->edges.empty()) {
            for (ScenarioTree::Edges::const_iterator it = pNode->edges.begin();
                    it != pNode->edges.end(); it++) {
                traversalStack.push(
                        ScenarioTree::NodeStateIndex(&(it->child),
                                stateIndex++));
            }
        }
    }

    if (pTree != NULL) {
        delete pTree;
    }

    return env;
}

void test(ScenarioTree* pTree, vector<double>& arrivalRates) {
    //ScenarioTree* pTree = hpModel->getEnvironmentProbabilityTree();
    //ScenarioTree tree = *pTree;

    stack<ScenarioTree::NodeStateIndex> traversalStack;
    int stateIndex = 0;

    traversalStack.push(
            ScenarioTree::NodeStateIndex(&(pTree->getRoot()), stateIndex++));

    while (!traversalStack.empty()) {
        const ScenarioTree::Node* pNode = traversalStack.top().pNode;
        //int nodeStateIndex = traversalStack.top().stateIndex;
        traversalStack.pop();
        std::cout << "HPAdaptationManager::test Pushing Predicted Value = " << 1/(pNode->value) << std::endl;
        arrivalRates.push_back(1/pNode->value);

        if (!pNode->edges.empty()) {
            // it's not a leaf node
            for (ScenarioTree::Edges::const_iterator it = pNode->edges.begin();
                    it != pNode->edges.end(); it++) {
                double high_probability = 0.63;
                if (it->probability >= high_probability) {
                    traversalStack.push(
                        ScenarioTree::NodeStateIndex(&(it->child),
                                stateIndex++));
                }
            }
        }
    }

    //if (pTree != NULL) {
    //    delete pTree;
    //}
}

void HPAdaptationManager::getMostLikelyPredictionsForHorizon(vector<double>& arrivalRates) {
    ScenarioTree* pTree = hpModel->getEnvironmentProbabilityTree();
    ScenarioTree tree = *pTree;

    stack<ScenarioTree::NodeStateIndex> traversalStack;
    int stateIndex = 0;

    traversalStack.push(
            ScenarioTree::NodeStateIndex(&tree.getRoot(), stateIndex++));

    // The first value is the current arrival rate not the predicted one
    bool skipFirst = true;

    while (!traversalStack.empty()) {
        const ScenarioTree::Node* pNode = traversalStack.top().pNode;
        //int nodeStateIndex = traversalStack.top().stateIndex;
        traversalStack.pop();

        if (!skipFirst) {
            //std::cout << "Pushing Predicted Value = " << 1/(pNode->value) << std::endl;
            arrivalRates.push_back(1/pNode->value);
        }

        skipFirst = false;

        if (!pNode->edges.empty()) {
            // it's not a leaf node
            for (ScenarioTree::Edges::const_iterator it = pNode->edges.begin();
                    it != pNode->edges.end(); it++) {
                double high_probability = 0.63;
                if (it->probability >= high_probability) {
                    traversalStack.push(
                        ScenarioTree::NodeStateIndex(&(it->child),
                                stateIndex++));
                }
            }
        }
    }

    if (pTree != NULL) {
        delete pTree;
    }
}

void HPAdaptationManager::getArrivalRates(std::vector<double>& arrivaRrates) {
    unsigned long history_length = 15;
    hpModel->getPastInterarrivalRates(arrivaRrates, history_length);
    //cout << arrivaRrates.size() << endl;
    getMostLikelyPredictionsForHorizon(arrivaRrates);
    //cout << arrivaRrates.size() << endl;
}

// This function returns the string for environment model
std::string HPAdaptationManager::getDTMC() {
    ScenarioTree* pTree = hpModel->getEnvironmentProbabilityTree();
    ScenarioTree tree = *pTree;

    const string STATE_VALUE_FORMULA = "formula stateValue = ";

    // Initialize
    EnvPredictionRecord::get_instance()->resetArrivalRates();

    string result;
    stringstream out;

    stack<ScenarioTree::NodeStateIndex> traversalStack;
    stringstream leaves;
    bool firstLeaf = true;
    stringstream values;

    int stateIndex = 0;
    string padding(STATE_VALUE_FORMULA.length(), ' ');

    traversalStack.push(
            ScenarioTree::NodeStateIndex(&tree.getRoot(), stateIndex++));
    while (!traversalStack.empty()) {
        const ScenarioTree::Node* pNode = traversalStack.top().pNode;
        int nodeStateIndex = traversalStack.top().stateIndex;
        traversalStack.pop();

        // collect state/value mappings
        if (nodeStateIndex > 0) {
            values << " + " << endl << padding;
        }
        values << "(" << STATE_VAR << " = " << nodeStateIndex << " ? "
                << pNode->value << " : 0)";

        EnvPredictionRecord::get_instance()->updateArrivalPredictions(pNode,
                nodeStateIndex);
        //dump_predictions(pNode->value, nodeStateIndex);

        if (pNode->edges.empty()) {

            // it's leaf node
            if (firstLeaf) {
                firstLeaf = false;
            } else {
                leaves << " | ";
            }
            leaves << STATE_VAR << " = " << nodeStateIndex;
        } else {
            out << GUARD << STATE_VAR << " = " << nodeStateIndex << " -> "
                    << endl;

            for (ScenarioTree::Edges::const_iterator it = pNode->edges.begin();
                    it != pNode->edges.end(); it++) {
                if (it != pNode->edges.begin()) {
                    out << endl << "\t+ ";
                } else {
                    out << '\t';
                }
                out << it->probability << " : (" << STATE_VAR << "' = "
                        << stateIndex << ")";
                traversalStack.push(
                        ScenarioTree::NodeStateIndex(&(it->child),
                                stateIndex++));
            }
            out << ';' << endl;
        }
    }

    // output transition for leaves of the probability tree
    if (!tree.getRoot().edges.empty()) {
        out << GUARD << "(" << leaves.str() << ") -> 1 : true;" << endl;
    } else {
        out << GUARD << "true -> 1 : true;" << endl;
    }

    out << "endmodule" << endl;

    // print stateValue formula
    stringstream stateVar;
    out << STATE_VALUE_FORMULA;
    if (tree.getRoot().edges.empty()) {
        out << tree.getRoot().value;
    } else {
        out << values.str();
        stateVar << STATE_VAR << " : [0.." << stateIndex - 1 << "] init 0;"
                << endl;
    }
    out << ";" << endl;
    result = "module environment\n" + stateVar.str() + out.str();

    //vector<double> arrivalRates;
    //test(pTree, arrivalRates);

    if (pTree != NULL) {
        delete pTree;
    }

    return result;
}

Tactic* HPAdaptationManager::evaluate() {
    MacroTactic* pMacroTactic = NULL;
    //return pMacroTactic;
    //ScenarioTree* pTree = hpModel->getEnvironmentProbabilityTree();
    //string environmentModel = getDTMC();
    //printf("%s\n", environmentModel.c_str());
    //string file_name = "/home/ashutosp/envModel";
    //std::ofstream myfile;

    //myfile.open (file_name, ios::app);
    //myfile << environmentModel << "\n";
    //myfile.close();
    //delete pTree;
    //return NULL;

    //TimeSeriesSimilarity::getInstance()->GetSimilarity();

    if (perfectEnv || usePredictor) {
        touch_util();
        touch_rt();
    }

    bool debug = true;

    clock_t t, t1;
    int f;

    t = clock();

    const Environment env = hpModel->getEnvironment();
    double arrival_rate = env.getArrivalMean();
    dump_arrival_rates(arrival_rate, hpModel);
    bool problemGenerationMode = omnetpp::getSimulation()->getSystemModule()->par(
            "problemGenerationMode").boolValue();
    bool classifierTestMode = omnetpp::getSimulation()->getSystemModule()->par(
            "classifierTestMode").boolValue();
    bool slow_planning_done = false;

    //int discretizedBrownoutFactor = 1
    //                        + (hpModel->getNumberOfBrownoutLevels() - 1)
    //                                * hpModel->getConfiguration().getBrownOutFactor();
    //cout << "discretizedBrownoutFactor = " << discretizedBrownoutFactor << endl;
    vector<string> actions;
    unsigned envIndex = UINT_MAX;
    double classifierLabel = -1.0;

    if (!usePredictor) {
        envIndex = NotDetEnvModel::get_instance()->getClosestArrivalRateIndex(
                1 / env.getArrivalMean(), true);
        if (envIndex == UINT_MAX) {
            NotDetEnvModel::get_instance()->updateModel(
                    1 / env.getArrivalMean());
        }
    }

    //cout << "environment Index = " << envIndex << "  " << 1/env.getArrivalMean() << endl;
    // Not the correct place to set this.
    hpModel->setCurrentTime(hpModel->getCurrentTime() + 1);
    //cout << "time = " << hpModel->getCurrentTime() << endl;

    // If already planned for environment then use existing plan
    if (mode == Fast || !PlanDB::get_instance()->get_plan(hpModel, actions)) {
        // Generate a new plan
        if (mode != Fast) {
            if (hpModel->isValidTime()) {
                dump_str("Plan Failed");
                cout << "Plan Failed" << endl;
            } else {
                dump_str("Plan Over");
                cout << "Plan Over" << endl;
            }
        }

        AdaptationPlanner planner;
        string templateName = par("templatePath");

        if (hpModel->getBootDelay() == 0) {
            templateName += "-nl";
        }

        templateName += ".prism";
        planner.setModelTemplatePath(templateName);

        string* pPath = new string;
        string fastPlanPath;
        string slowPlanPath;

        string pathToStoreProfilingProblems = "";

        if (problemGenerationMode || classifierTestMode) {
            pathToStoreProfilingProblems = omnetpp::getSimulation()->getSystemModule()->par(
                    "pathToDumpProfilingProblems").stdstringValue();
        }

        stringstream initialState;
        stringstream environmentModel;

        bool trigger_fast_planning = false;
        std::vector<double> arrivalRates;
        getArrivalRates(arrivalRates);
        bool useIBL = omnetpp::getSimulation()->getSystemModule()->par(
                "useIBL").boolValue();

        if ((!problemGenerationMode && useIBL) || classifierTestMode) {
            classifierLabel  = ClassifyProblemInstance::getInstance()->useReactive(hpModel, arrivalRates);
        }

        // TODO deal with false condition
        if ((!problemGenerationMode && !useIBL && mode == Hybrid && hpModel->getObservations().avgResponseTime > RT_THRESHOLD)
                || problemGenerationMode
                || (!problemGenerationMode && useIBL
                        && (classifierLabel == 1.0))
                || classifierTestMode) {
        //if (mode == Hybrid && MGCQueue::getResponseTime(hpModel, getNextEnvironmentPrediction()) > RT_THRESHOLD) {
            //if (envIndex == UINT_MAX) {
            //if (envIndex == UINT_MAX || hpModel->getObservations().avgResponseTime > RT_THRESHOLD) {
            // hpModel->getObservations().avgResponseTime > RT_THRESHOLD
            trigger_fast_planning = true;
        }

        // Fix conditions
        if (mode == Fast || ((mode == Hybrid || problemGenerationMode) && trigger_fast_planning)
                || ((mode == Hybrid || classifierTestMode) && trigger_fast_planning)) {
            // In hybrid mode trigger fast planning only if environment model fails.
            // Fast Planning
            get_initial_state_str(initialState);

            if (usePredictor) {
                environmentModel << "formula stateValue = "
                        << getNextEnvironmentPrediction() << ";" << endl; //TODO uncomment this
                //environmentModel << "formula stateValue = " << arrival_rate << ";" << endl;
                //environmentModel << getDTMC(); // TODO Ashutosh Remove this and uncomment previous statement
            } else {
                environmentModel << "formula stateValue = " << arrival_rate
                        << ";" << endl;
            }

            cout << "Fast Planning Triggered" << endl;
            dump_str("Fast Planning Triggered");
            //cout << "Response Time = " << hpModel->getObservations().avgResponseTime << endl;

            actions = planner.plan(environmentModel.str(), initialState.str(),
                    pPath, true);
            //cout << "action size = " << actions.size() << endl;
            //Verification verify = Verification(initialState.str(), getDTMC(), *pPath); // TODO do not call getDTMC() multiple times.
            //verify.ExecuteActions(actions);
            dump_str(pPath->c_str());
            fastPlanPath = *pPath;

            // If classifier says don't use fast then clear the actions but still dump fast planning files. Only for IBL0
            if (classifierTestMode && classifierLabel != 1.0) {
                actions.clear();
            }

            if (debug) {
                //cout << "Fast Plan Path = " << *pPath << endl;
            }
        }

        //ptime startTime = microsec_clock::local_time();
        //cout << "Simtime = " << simTime() << endl;

        if (mode != Fast || problemGenerationMode) {
            // Slow Planning
            environmentModel.str("");
            environmentModel.clear();
            initialState.str("");
            initialState.clear();
            get_initial_state_str(initialState, false);
            hpModel->resetCurrentTime();

            if (usePredictor) {
                environmentModel << getDTMC();
                if (pathToStoreProfilingProblems != "") {
                    environmentModel << "// #ENV ENDS" << endl;
                }
            } else {
                NotDetEnvModel::get_instance()->getEnvPrismModel(arrival_rate,
                        environmentModel);
            }
            //cout << environmentModel.str() << endl;

            cout << "Slow Planning Triggered" << endl;
            dump_str("Slow Planning Triggered");

            planner.plan(environmentModel.str(), initialState.str(), pPath,
                    false);
            slow_planning_done = true;
            if (debug) {
                //t = clock() - t;
                //printf("Planning Time = %l clicks (%f seconds).\n", t,
                //        ((float) t) / CLOCKS_PER_SEC);
                //t1 = clock();
            }

            PlanDB::get_instance()->clean_db();
            PlanDB::get_instance()->populate_db(pPath->c_str());
            slowPlanPath = *pPath;
            dump_str(pPath->c_str());

            if (pathToStoreProfilingProblems != "") {
                PredictableSource *source = check_and_cast<PredictableSource *>
                        (getParentModule()->getSubmodule("source"));
                string trace_name = source->getSourceFile();
                DumpPlanningProblems::get_instance(pathToStoreProfilingProblems)
                        ->copySampleProblems(fastPlanPath, slowPlanPath, hpModel,
                                arrivalRates, classifierLabel, trace_name);
            }
        }

        //cout << "Planning Time = " << (microsec_clock::local_time() - startTime).total_milliseconds()
        //        << "Simtime" << simTime() << endl;
        if (debug) {
            t1 = clock() - t1;
            //printf("Loading time = %l clicks (%f seconds).\n", t1,
            //        ((float) t1) / CLOCKS_PER_SEC);
        }

        if (pPath) {
            //cout << "Slow Plan Path " << *pPath << endl;
            delete pPath;
        }

        //if (mode == OnlySlowPlanner) {
        //    PlanDB::get_instance()->get_plan(hpModel, actions);
        //}
    }

    // Assumption is that with slow planner, the plan will be available in the next adaptation cycle.
    pMacroTactic = new MacroTactic;

/*    static bool hack = true;

    double arrival = 83.8415;
    std::cout << "Difference = " << (double(1/arrival_rate) - arrival) << std::endl;
    double p = (double(1/arrival_rate) - arrival);
    double q = p < 0 ? -p : p;

    if (hack && q <= 0.001) {
        actions.clear();
        actions.push_back("removeServerC_start");
        //actions.push_back("decreaseDimmer_start");

        actions.push_back("divert_75_25_0");
        hack = false;
    }*/

    if (slow_planning_done && omnetpp::getSimulation()->getSystemModule()->par("mode").stdstringValue() == "Slow_instant") {
        if (!PlanDB::get_instance()->get_plan(hpModel, actions)) {
            assert(false);
        }
    }

    extract_tactics(actions, pMacroTactic);

    if (pMacroTactic != NULL && pMacroTactic->isEmpty()) {
        delete pMacroTactic;
        pMacroTactic = 0;
    }

    return pMacroTactic;
}

void HPAdaptationManager::extract_tactics(const vector<string> &actions,
        MacroTactic* pMacroTactic) {
    assert(pMacroTactic != NULL);
    vector<string>::const_iterator it = actions.begin();

    for (; it != actions.end(); it++) {
        int discretizedBrownoutFactor = hpModel->getDimmerLevel();
        printf("%s\n", it->c_str());
        dump_str(it->c_str());
        if (it->compare("addServerA_start") == 0) {
            pMacroTactic->addTactic(
                    new HPAddServerTactic(MTServerAdvance::ServerType::A));
        } else if (it->compare("addServerB_start") == 0) {
            pMacroTactic->addTactic(
                    new HPAddServerTactic(MTServerAdvance::ServerType::B));
        } else if (it->compare("addServerC_start") == 0) {
            pMacroTactic->addTactic(
                    new HPAddServerTactic(MTServerAdvance::ServerType::C));
        } else if (it->compare("removeServerA_start") == 0) {
            pMacroTactic->addTactic(
                    new HPRemoveServerTactic(MTServerAdvance::ServerType::A));
        } else if (it->compare("removeServerB_start") == 0) {
            pMacroTactic->addTactic(
                    new HPRemoveServerTactic(MTServerAdvance::ServerType::B));
        } else if (it->compare("removeServerC_start") == 0) {
            pMacroTactic->addTactic(
                    new HPRemoveServerTactic(MTServerAdvance::ServerType::C));
        } else if (it->compare(0, 3, "dec") == 0) {
            discretizedBrownoutFactor--;
            double newBrownoutFactor = (discretizedBrownoutFactor - 1.0)
                    / (hpModel->getNumberOfBrownoutLevels() - 1.0);
            pMacroTactic->addTactic(new SetBrownoutTactic(newBrownoutFactor));
        } else if (it->compare(0, 3, "inc") == 0) {
            discretizedBrownoutFactor++;
            double newBrownoutFactor = (discretizedBrownoutFactor - 1.0)
                    / (hpModel->getNumberOfBrownoutLevels() - 1.0);
            pMacroTactic->addTactic(new SetBrownoutTactic(newBrownoutFactor));
        } else if (it->compare("divert_100_0_0") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::HUNDRED,
                            HPLoadBalancer::TrafficLoad::ZERO,
                            HPLoadBalancer::TrafficLoad::ZERO));
        } else if (it->compare("divert_75_25_0") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(
                            HPLoadBalancer::TrafficLoad::SEVENTYFIVE,
                            HPLoadBalancer::TrafficLoad::TWENTYFIVE,
                            HPLoadBalancer::TrafficLoad::ZERO));
        } else if (it->compare("divert_75_0_25") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(
                            HPLoadBalancer::TrafficLoad::SEVENTYFIVE,
                            HPLoadBalancer::TrafficLoad::ZERO,
                            HPLoadBalancer::TrafficLoad::TWENTYFIVE));
        } else if (it->compare("divert_50_50_0") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::FIFTY,
                            HPLoadBalancer::TrafficLoad::FIFTY,
                            HPLoadBalancer::TrafficLoad::ZERO));
        } else if (it->compare("divert_50_0_50") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::FIFTY,
                            HPLoadBalancer::TrafficLoad::ZERO,
                            HPLoadBalancer::TrafficLoad::FIFTY));
        } else if (it->compare("divert_50_25_25") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::FIFTY,
                            HPLoadBalancer::TrafficLoad::TWENTYFIVE,
                            HPLoadBalancer::TrafficLoad::TWENTYFIVE));
        } else if (it->compare("divert_25_75_0") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::TWENTYFIVE,
                            HPLoadBalancer::TrafficLoad::SEVENTYFIVE,
                            HPLoadBalancer::TrafficLoad::ZERO));
        } else if (it->compare("divert_25_0_75") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::TWENTYFIVE,
                            HPLoadBalancer::TrafficLoad::ZERO,
                            HPLoadBalancer::TrafficLoad::SEVENTYFIVE));
        } else if (it->compare("divert_25_50_25") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::TWENTYFIVE,
                            HPLoadBalancer::TrafficLoad::FIFTY,
                            HPLoadBalancer::TrafficLoad::TWENTYFIVE));
        } else if (it->compare("divert_25_25_50") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::TWENTYFIVE,
                            HPLoadBalancer::TrafficLoad::TWENTYFIVE,
                            HPLoadBalancer::TrafficLoad::FIFTY));
        } else if (it->compare("divert_0_100_0") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::ZERO,
                            HPLoadBalancer::TrafficLoad::HUNDRED,
                            HPLoadBalancer::TrafficLoad::ZERO));
        } else if (it->compare("divert_0_0_100") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::ZERO,
                            HPLoadBalancer::TrafficLoad::ZERO,
                            HPLoadBalancer::TrafficLoad::HUNDRED));
        } else if (it->compare("divert_0_75_25") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::ZERO,
                            HPLoadBalancer::TrafficLoad::SEVENTYFIVE,
                            HPLoadBalancer::TrafficLoad::TWENTYFIVE));
        } else if (it->compare("divert_0_25_75") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::ZERO,
                            HPLoadBalancer::TrafficLoad::TWENTYFIVE,
                            HPLoadBalancer::TrafficLoad::SEVENTYFIVE));
        } else if (it->compare("divert_0_50_50") == 0) {
            pMacroTactic->addTactic(
                    new HPDivertTraffic(HPLoadBalancer::TrafficLoad::ZERO,
                            HPLoadBalancer::TrafficLoad::FIFTY,
                            HPLoadBalancer::TrafficLoad::FIFTY));
        } else if (it->compare(0, 8, "progress") == 0 || it->compare("addServerA_complete") == 0
                || it->compare("addServerB_complete") == 0 || it->compare("addServerC_complete") == 0) {
        } else {
            ASSERT(false); // unknown tactic
        }
    }
}

