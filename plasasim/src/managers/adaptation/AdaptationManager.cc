/**
 * Copyright (c) 2015 Carnegie Mellon University. All Rights Reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:

 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following acknowledgments
 * and disclaimers.

 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.

 * 3. The names "Carnegie Mellon University," "SEI" and/or "Software
 * Engineering Institute" shall not be used to endorse or promote
 * products derived from this software without prior written
 * permission. For written permission, please contact
 * permission@sei.cmu.edu.

 * 4. Products derived from this software may not be called "SEI" nor
 * may "SEI" appear in their names without prior written permission of
 * permission@sei.cmu.edu.

 * 5. Redistributions of any form whatsoever must retain the following
 * acknowledgment:

 * This material is based upon work funded and supported by the
 * Department of Defense under Contract No. FA8721-05-C-0003 with
 * Carnegie Mellon University for the operation of the Software
 * Engineering Institute, a federally funded research and development
 * center.

 * Any opinions, findings and conclusions or recommendations expressed
 * in this material are those of the author(s) and do not necessarily
 * reflect the views of the United States Department of Defense.

 * NO WARRANTY. THIS CARNEGIE MELLON UNIVERSITY AND SOFTWARE
 * ENGINEERING INSTITUTE MATERIAL IS FURNISHED ON AN "AS-IS"
 * BASIS. CARNEGIE MELLON UNIVERSITY MAKES NO WARRANTIES OF ANY KIND,
 * EITHER EXPRESSED OR IMPLIED, AS TO ANY MATTER INCLUDING, BUT NOT
 * LIMITED TO, WARRANTY OF FITNESS FOR PURPOSE OR MERCHANTABILITY,
 * EXCLUSIVITY, OR RESULTS OBTAINED FROM USE OF THE MATERIAL. CARNEGIE
 * MELLON UNIVERSITY DOES NOT MAKE ANY WARRANTY OF ANY KIND WITH
 * RESPECT TO FREEDOM FROM PATENT, TRADEMARK, OR COPYRIGHT
 * INFRINGEMENT.

 * This material has been approved for public release and unlimited
 * distribution.

 * DM-0002494
**/

#include "AdaptationManager.h"
#include "ModulePriorities.h"
#include <Plan/PlanDB.h>
#include <iostream>

using namespace boost::posix_time;

AdaptationManager::AdaptationManager() :
        periodEvent(0), mode(Slow) {
}

void AdaptationManager::initialize(int stage) {
    if (stage == 0) {
        // Hack
        cModule* mod = getParentModule();
        //std::cout << mod->getFullPath() << "####" << mod->getFullName() << endl;
        if (strcmp(mod->getFullPath().c_str(), "HPBrownout") == 0) {
            hpModel = check_and_cast<HPModel*>(
                    getParentModule()->getSubmodule("model"));
            //pModel = NULL;
        } else {
            assert(false);
        }
            //else {
            //pModel = check_and_cast<educscmu::Model*>(
            //        getParentModule()->getSubmodule("model"));
            //hpModel = NULL;
        //}
        decisionTimeSignal = registerSignal("decisionTime");
    } else {

        // Create the event object we'll use for timing -- just any ordinary message.
        periodEvent = new cMessage("adaptMgrPeriod");
        periodEvent->setSchedulingPriority(ADAPTATION_MGR_PRIO);

        scheduleAt(
                omnetpp::getSimulation()->getWarmupPeriod()
                + hpModel->getEvaluationPeriod(), periodEvent);
        if (mode != Fast) {
                //PlanDB::get_instance()->populate_db(
                //        "/home/ashutosp/Dropbox/Sandbox/plasasim/simulations/brownout/InitialPlan");
        }
    }
}

void AdaptationManager::handleMessage(cMessage *msg) {
    //static Tactic* prevTactic = NULL;

    Tactic* pTactic = NULL;
    ExecutionManager* pExecMgr = NULL;

    /*    if (mode != OnlySlowPlanner && prevTactic != NULL) {
     pExecMgr = check_and_cast<ExecutionManager*>(
     getParentModule()->getSubmodule("executionManager"));
     prevTactic->execute(pExecMgr);
     delete prevTactic;
     prevTactic = NULL;
     }*/

    double delay = 0;

    if (pTactic == NULL) {
        //std::cout << "AdaptationManager::handleMessage Msg = " << msg->getName()
        //        << std::endl;
        ptime startTime = microsec_clock::local_time();
        pTactic = evaluate();
        delay = (microsec_clock::local_time() - startTime).total_seconds();
        //cout << "Planning Time = "
        //        << (microsec_clock::local_time() - startTime).total_seconds()
        //        << " Simtime = " << simTime() << endl;
        emit(decisionTimeSignal,
                double((microsec_clock::local_time() - startTime).total_milliseconds()));
    }

    //assert(delay <= 60);
    //cout << "delay = " << delay << endl;

    // Assumption is that with slow planner, the plan will be available in the next adaptation cycle.
    if (pTactic != NULL) {
        std::cout << "AdaptationMgr simtime = " << simTime() << " tactic = "
                << *pTactic << std::endl;

        if (pExecMgr == NULL) {
            pExecMgr = check_and_cast<ExecutionManager*>(
                    getParentModule()->getSubmodule("executionManager"));
        }

        pTactic->execute(pExecMgr);
        delete pTactic;
        pTactic = NULL;
    }

    //if (pModel != NULL) {
    //    scheduleAt(simTime() + pModel->getEvaluationPeriod(), periodEvent);
    //} else {
        scheduleAt(simTime() + hpModel->getEvaluationPeriod(), periodEvent);
    //}
}

AdaptationManager::~AdaptationManager() {
    cancelAndDelete(periodEvent);
}
