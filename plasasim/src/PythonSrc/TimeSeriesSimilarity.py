from pydtw import dtw1d
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
import numpy as np

#def TimeSeriesSimilarity(seriesA,seriesB, distanceMetric):
def TimeSeriesSimilarity(seriesA, seriesB):
    #distanceMetric = "euclidean"
    
    #print("TimeSeriesSimilarity")
    #print(seriesA)
    #print(seriesB)
    
    #a = np.array(seriesA)
    #b = np.array(seriesB)
    #cost_matrix, alignmend_a, alignmend_b = dtw1d(a, b)
    #print(alignmend_a)
    #print("Calling fast DTW")
    distance, path = fastdtw(seriesA, seriesB, dist=euclidean)
    
    return distance

