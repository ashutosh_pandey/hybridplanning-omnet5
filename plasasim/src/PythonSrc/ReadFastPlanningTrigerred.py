import sys
from collections import defaultdict

def get_info(path):
    elements = path.split('/')

    return elements[0], elements[1]
    
def main():
    file = open("FastPlanningTrigerred", "r")
    #print("File = ", file) 
    data_base = defaultdict(list)
    fast_count_writer = open("FastPlanningCount.csv", "w+")
    fast_count_writer.write("Trace, Fast, Slow, Hybrid, Hybrid_IBL0, Hybrid_IBL1\n")

    for line in file: 
        #print(line)
        day, mode = get_info(line)

        if day not in data_base.keys():
            data_base[day] = [0, 0, 0, 0, 0] # -1 because first one failed by default


        if (mode == "Fast"):
            (data_base[day])[0] = (data_base[day])[0] + 1
        if (mode == "Slow"):
            (data_base[day])[1] = (data_base[day])[1] + 1
        if (mode == "Hybrid"):
            (data_base[day])[2] = (data_base[day])[2] + 1
        if (mode == "Hybrid_IBL0"):
            (data_base[day])[3] = (data_base[day])[3] + 1
        if (mode == "Hybrid_IBL1"):
            (data_base[day])[4] = (data_base[day])[4] + 1

    file.close()

    # Now dump the data
    for day in data_base.keys():
        line_str = day + ", " +  str((data_base[day])[0]) \
                + ", " +  str((data_base[day])[1]) \
                + ", " +  str((data_base[day])[2]) \
                + ", " +  str((data_base[day])[3]) \
                + ", " +  str((data_base[day])[4]) + "\n"

        fast_count_writer.write(line_str)

    
    fast_count_writer.close()


if __name__ == "__main__":
    sys.exit(main())
