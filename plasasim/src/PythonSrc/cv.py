import sys
#from collections import defaultdict
#from ProblemDatabase import ProblemFeatures
#from ProblemDatabase import ProblemDatabase
#from ProblemDatabase import ProblemInstance
from DBWrapper import DBWrapper
#from ProblemDatabase import *
#from ProblemDB import *
#from sklearn.neighbors import KNeighborsClassifier
#from sklearn.neighbors import RadiusNeighborsClassifier
import random
#from sklearn import svm
from sklearn.metrics import f1_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import confusion_matrix
#from sklearn import preprocessing
#from sklearn.model_selection import train_test_split
#from sklearn.model_selection import KFold
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_validate
#from sklearn.linear_model import LogisticRegression
#from sklearn.linear_model import LogisticRegressionCV
#from sklearn.neural_network import MLPClassifier
#from sklearn.ensemble import RandomForestClassifier
#from sklearn.ensemble import AdaBoostClassifier
#from sklearn.ensemble import GradientBoostingClassifier
#from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import ExtraTreesClassifier
#from sklearn.svm import LinearSVC
#from sklearn.tree import DecisionTreeClassifier
from statistics import *
#import numpy

def get_random_numbers(minimum, maximum, count):
    random_numbers = set()
        
    for x in range(count):
        random_numbers.add(random.randint(minimum, maximum))

    return random_numbers

class CrossValidate:
    #__slots__ = ['__features_list', '__labels_list', '__problems']
    
    def __init__(self, data_file, fold, iterations, skip_traces, \
                compare_past_workload_for_similarity = 1, debug_file = "", cv_file = ""):
        self.__data_file = data_file
        self.__fold = int(fold)
        self.__iterations = int(iterations)
        self.__skip_traces = skip_traces
        self.__compare_past_workload_for_similarity = compare_past_workload_for_similarity
        self.__db = DBWrapper(data_file, skip_traces, compare_past_workload_for_similarity)
        #self.__debug_file_name = str(fold) + "_ExtraTreesClassifier_log.csv"
        self.__file = debug_file
        #self.__cv_file_name = str(fold) + "_cv_ExtraTreesClassifier_log.csv"
        self.__cv_file = cv_file
        #open("cv_log", "w")
 
    def get_all_traces(self):
        return self.__db.get_all_traces()

    def cv_per_trace(self):
        print("############ ERROR: cv_per_trace NOT IMPLEMENTED ##############")
        assert False
        return 0, 0

    def cv_per_problem(self):
        all_training_problems = self.__db.get_all_training_problems()
        scoring = ['accuracy', 'precision_micro', 'recall_micro', 'f1_micro', \
                    'precision_macro', 'recall_macro', 'f1_macro', \
                    'precision_weighted', 'recall_weighted', 'f1_weighted']
        #clf = svm.SVC(kernel='linear', C=0.5)
        #clf = LinearSVC(random_state=0, C=0.000001) 
        #multi_class='crammer_singer')
        #clf = svm.SVC(C=500, decision_function_shape='ovr')
        #clf = KNeighborsClassifier(n_neighbors = 3, algorithm = 'ball_tree')
        #clf = RadiusNeighborsClassifier(n_neighbors = 3, algorithm = 'ball_tree')
        #clf = LogisticRegression(C=1.0, solver='newton-cg', multi_class='multinomial')
        #clf = LogisticRegression(C=1.0, solver='sag', multi_class='multinomial')
        #clf = LogisticRegression(solver='sag')
        #clf = LogisticRegression(solver='saga')
        #clf = LogisticRegression(solver='lbfgs')
        #clf = LogisticRegressionCV(solver='newton-cg')
        #clf = MLPClassifier(solver='sgd')
        #clf = DecisionTreeClassifier(random_state=0)
        #clf = RandomForestClassifier(max_depth=20, random_state=0)
        #clf = AdaBoostClassifier()
        #clf = GradientBoostingClassifier(max_depth=3)
        #clf = BaggingClassifier(n_estimators=20)

        #n_estimators = [25, 50, 75, 100, 125, 150, 175, 200, 225, 250]
        #criterion = ['gini', 'entropy']
        #max_depth = [None, 20, 30, 50]
        #min_samples_split = [2, 12, 22]
        #max_features = [None, 'log2', 'sqrt', 'auto']

        n_estimators = [80, 83, 85, 87, 90, 91, 93, 95, 97, 100, 102, 105, 107, 110]
        criterion = ['gini']
        max_depth = [None]
        min_samples_split = [2]
        max_features = ['auto']
        #loss = ['deviance']

        # ExtraTree
        self.__file.write("n_estimators, criterion, max_depth, min_samples_split, max_features, \
                        recall_score_0, recall_score_1, recall_score_2, precision_score_0, precision_score_1, precision_score_2, \
                        f1_score_0, f1_score_1, f1_score_2, recall_score_macro, recall_score_micro, precision_score_macro, \
                        precision_score_micro, f1_score_macro, f1_score_micro\n")
        #self.__file.write("n_estimators, loss, max_depth, min_samples_split, max_features, \
        #                recall_score_0, recall_score_1, recall_score_2, precision_score_0, precision_score_1, precision_score_2, \
        #                f1_score_0, f1_score_1, f1_score_2, recall_score_macro, recall_score_micro, precision_score_macro, \
        #                precision_score_micro, f1_score_macro, f1_score_micro\n")
        self.__cv_file.write("n_estimators, criterion, max_depth, min_samples_split, max_features, \
                        recall_0_avg, recall_0_dev, recall_1_avg, recall_1_dev, recall_2_avg, recall_2_dev,\
                        recall_macro, recall_micro, recall_weighted, precision_0_avg, precision_0_dev, \
                        precision_1_avg, precision_1_dev, precision_2_avg, precision_2_dev, \
                        precision_macro, precision_micro, precision_weighted, f1_0_avg, f1_0_dev, \
                        f1_1_avg, f1_1_dev, f1_2_avg, f1_2_dev, f1_macro, f1_micro, f1_weighted\n")

        #GradientBoostingClassifier(loss=’deviance’, learning_rate=0.1, n_estimators=100, \
                #subsample=1.0, criterion=’friedman_mse’, min_samples_split=2, min_samples_leaf=1, \
                #min_weight_fraction_leaf=0.0, max_depth=3, min_impurity_decrease=0.0, \
                #min_impurity_split=None, init=None, random_state=None, max_features=None, \
                #verbose=0, max_leaf_nodes=None, warm_start=False, presort=’auto’)
        max_recall_0_mean = 0
        best_estimator_count = 10 # Start with the default value

        for estimator in n_estimators:
            for c in criterion:
                for depth in max_depth:
                    for sample_split in min_samples_split:
                        for features in max_features:
                            recall_0 = []
                            recall_1 = []
                            recall_2 = []

                            precision_0 = []
                            precision_1 = []
                            precision_2 = []

                            f1_0 = []
                            f1_1 = []
                            f1_2 = []

                            clf = ExtraTreesClassifier(n_estimators=estimator, criterion=c, \
                                    max_depth=depth, min_samples_split=sample_split, \
                                    min_samples_leaf=1, min_weight_fraction_leaf=0.0, \
                                    max_features=features, max_leaf_nodes=None, \
                                    min_impurity_decrease=0.0, min_impurity_split=None, \
                                    bootstrap=False, oob_score=False, \
                                    n_jobs=1, random_state=None, verbose=0, \
                                    warm_start=False, class_weight=None)
        
                            #clf = GradientBoostingClassifier(loss=l, learning_rate=0.1, n_estimators=estimator, \
                            #        subsample=1.0, criterion='friedman_mse', min_samples_split=sample_split, min_samples_leaf=1, \
                            #        min_weight_fraction_leaf=0.0, max_depth=depth, min_impurity_decrease=0.0, \
                            #        min_impurity_split=None, init=None, random_state=None, max_features=features, \
                            #        verbose=0, max_leaf_nodes=None, warm_start=False, presort='auto')
                            skf = StratifiedKFold(n_splits=self.__fold)
                            skf.get_n_splits(all_training_problems.get_features_list(), \
                                    all_training_problems.get_label_list())
                            features_list = all_training_problems.get_features_list()
                            labels_list = all_training_problems.get_label_list()
                            fold = 0
                            for train_index, test_index in skf.split(all_training_problems.get_features_list(), \
                                    all_training_problems.get_label_list()):
                                #print("TRAIN:", train_index, "TEST:", test_index)
            
                                tr_features = list()
                                tr_labels = list()
                                tt_features = list()
                                tt_labels = list()

                                for i in train_index:
                                    tr_features.append(features_list[i])
                                    tr_labels.append(labels_list[i])

                                for i in test_index:
                                    tt_features.append(features_list[i])
                                    tt_labels.append(labels_list[i])

                                #print(tr_features)
                                clf.fit(tr_features, tr_labels)
                                predicted_labels = list()
                                train_predicted_labels = list()

                                #for problem in tr_features:
                                #    predicted_label = clf.predict([problem])
                                #    train_predicted_labels.append(predicted_label)

                                #i = 0
                                #while i < len(train_predicted_labels):
                                    #print("Predicted,  Actual ", train_predicted_labels[i], tr_labels[i])
                                #    i = i + 1

                                #print("Confusion Matrix for train data = ", confusion_matrix(tr_labels, train_predicted_labels))

                                for problem in tt_features:
                                    #print(problem)
                                    predicted_label = clf.predict([problem])
                                    predicted_labels.append(predicted_label)

                                i = 0
                                #   while i < len(predicted_labels):
                                #print("Predicted,  Actual ", predicted_labels[i], tt_labels[i])
                                #       i = i + 1

                                print("&&&&&&&&&& Fold = ", fold, "&&&&&&&&&&")
                                fold = fold + 1
                                print("Confusion Matrix = ", confusion_matrix(tt_labels, predicted_labels))
            
                                recall = recall_score(tt_labels, predicted_labels, average=None)
                                print("recall_score = ", recall)

                                prec_score = precision_score(tt_labels, predicted_labels, average=None)
                                print("precision_score = ", prec_score)
                               
                                f1 = f1_score(tt_labels, predicted_labels, average=None)
                                print("f1_score = ", f1)
                                   
                                recall_macro = recall_score(tt_labels, predicted_labels, average='macro')
                                print("recall_score_macro = ", recall_macro)
                                
                                recall_micro = recall_score(tt_labels, predicted_labels, average='micro')
                                print("recall_score_micro = ", recall_micro)
                                
                                precision_macro = precision_score(tt_labels, predicted_labels, average='macro')    
                                print("precision_score_macro = ", precision_macro)
                                    
                                precision_micro = precision_score(tt_labels, predicted_labels, average='micro')    
                                print("precision_score_micro = ", precision_micro)
                                   
                                f1_macro = f1_score(tt_labels, predicted_labels, average='macro') 
                                print("f1_score_macro = ", f1_macro)
                                    
                                f1_micro = f1_score(tt_labels, predicted_labels, average='micro')
                                print("f1_score_micro = ", f1_micro)
                                print("&&&&&&&&&&&&&&&&&&&&&&&&&")
                                recall_0.append(recall[0])
                                recall_1.append(recall[1])
                                recall_2.append(recall[2])

                                precision_0.append(prec_score[0])
                                precision_1.append(prec_score[1])
                                precision_2.append(prec_score[2])

                                f1_0.append(f1[0])
                                f1_1.append(f1[1])
                                f1_2.append(f1[2])
                                #row = str(estimator) + ", " + str(c) + ", " + str(depth) + ", " \
                                #    + str(sample_split) + ", " + str(features) + ", " \
                                #    + str(recall[0]) + ", " + str(recall[1])  + ", " + str(recall[2]) + ", " \
                                #    + str(prec_score[0]) + ", " + str(prec_score[1])  + ", " + str(prec_score[2]) + ", " \
                                #    + str(f1[0]) + ", " + str(f1[1])  + ", " + str(f1[2]) + ", " \
                                #    + str(recall_macro) + ", " + str(recall_micro) + ", " + str(precision_macro) + ", " \
                                #    + str(precision_micro) + ", " + str(f1_macro) + ", " + str(f1_micro) +  '\n'

                                row = str(estimator) + ", " + str(c) + ", " + str(depth) + ", " \
                                    + str(sample_split) + ", " + str(features) + ", " \
                                    + str(recall[0]) + ", " + str(recall[1])  + ", " + str(recall[2]) + ", " \
                                    + str(prec_score[0]) + ", " + str(prec_score[1])  + ", " + str(prec_score[2]) + ", " \
                                    + str(f1[0]) + ", " + str(f1[1])  + ", " + str(f1[2]) + ", " \
                                    + str(recall_macro) + ", " + str(recall_micro) + ", " + str(precision_macro) + ", " \
                                    + str(precision_micro) + ", " + str(f1_macro) + ", " + str(f1_micro) +  '\n'
                                self.__file.write(row)
                                #self.__cv_file.write(row)

                            scores = cross_validate(clf, all_training_problems.get_features_list(), \
                                    all_training_problems.get_label_list(), scoring=scoring, cv=skf, return_train_score=True)
                            #print("score keys = ", sorted(scores.keys()))
                            #print("f1_score_micro = ", scores['test_f1_micro'])
                            print(str(estimator) + ", " + str(c) + ", " + str(depth) + ", " \
                                    + str(sample_split) + ", " + str(features))
                            print("*******************************************************")
                            recall_0_mean = mean(recall_0)
                            print("recall_0_mean = ", recall_0_mean)
                            recall_0_dev = stdev(recall_0)
                            print("recall_0_dev = ", recall_0_dev)
                            
                            recall_1_mean = mean(recall_1)
                            print("recall_1_mean = ", recall_1_mean)
                            recall_1_dev = stdev(recall_1)
                            print("recall_1_dev = ", recall_1_dev)
                            
                            recall_2_mean = mean(recall_2)
                            print("recall_2_mean = ", recall_2_mean)
                            recall_2_dev = stdev(recall_2)
                            print("recall_2_dev = ", recall_2_dev)
                            
                            #print("recall_macro = ", scores['test_recall_macro'])
                            recall_macro_mean = scores['test_recall_macro'].mean()
                            print("recall_macro_mean = ", recall_macro_mean)
                            #print("recall_macro_std = ", scores['test_recall_macro'].std())

                            #print("recall_micro = ", scores['test_recall_micro'])
                            recall_micro_mean = scores['test_recall_micro'].mean()
                            print("recall_micro_mean = ", recall_micro_mean)
                            #print("recall_micro_std = ", scores['test_recall_micro'].std())
        
                            #print("recall_weighted = ", scores['test_recall_weighted'])
                            recall_weighted_mean = scores['test_recall_weighted'].mean()
                            print("recall_weighted_mean = ", recall_weighted_mean)
                            #print("recall_weighted_std = ", scores['test_recall_weighted'].std())
                            
                            print("#########################")
                            precision_0_mean = mean(precision_0)
                            print("precision_0_mean = ", precision_0_mean)
                            precision_0_dev = stdev(precision_0)
                            print("precision_0_dev = ", precision_0_dev)
                            
                            precision_1_mean = mean(precision_1)
                            print("precision_1_mean = ", precision_1_mean)
                            precision_1_dev = stdev(precision_1)
                            print("precision_1_dev = ", precision_1_dev)
                            
                            precision_2_mean = mean(precision_2)
                            print("precision_2_mean = ", precision_2_mean)
                            precision_2_dev = stdev(precision_2)
                            print("precision_2_dev = ", precision_2_dev)
                            
                            #print("precision_macro = ", scores['test_precision_macro'].mean())
                            precision_macro_mean = scores['test_precision_macro'].mean()
                            print("precision_macro_mean = ", precision_macro_mean)
                            #print("precision_macro_std = ", scores['test_precision_macro'].std())
        
                            #print("precision_micro = ", scores['test_precision_micro'].mean())
                            precision_micro_mean = scores['test_precision_micro'].mean()
                            print("precision_micro_mean = ", precision_micro_mean)
                            #print("precision_micro_std = ", scores['test_precision_micro'].std())
        
                            #print("precision_weighted = ", scores['test_precision_weighted'])
                            precision_weighted_mean = scores['test_precision_weighted'].mean()
                            print("precision_weighted_mean = ", precision_weighted_mean)
                            #print("precision_weighted_std = ", scores['test_precision_weighted'].std())
                            print("#########################")
                            f1_0_mean = mean(f1_0)
                            print("f1_0_mean = ", f1_0_mean)
                            f1_0_dev = stdev(f1_0)
                            print("f1_0_dev = ", f1_0_dev)
                            
                            f1_1_mean = mean(f1_1)
                            print("f1_1_mean = ", f1_1_mean)
                            f1_1_dev = stdev(f1_1)
                            print("f1_1_dev = ", f1_1_dev)
                            
                            f1_2_mean = mean(f1_2)
                            print("f1_2_mean = ", f1_2_mean)
                            f1_2_dev = stdev(f1_2)
                            print("f1_2_dev = ", f1_2_dev)
                            
                            f1_score_macro_mean = (scores['test_f1_macro']).mean()
                            print("f1_score_macro mean = ", f1_score_macro_mean)
                            #print("f1_score_macro std = ", (scores['test_f1_macro']).std())
        
                            f1_score_micro_mean = (scores['test_f1_micro']).mean()
                            print("f1_score_micro mean = ", f1_score_micro_mean)
                            #print("f1_score_micro std = ", (scores['test_f1_micro']).std())
        
                            f1_score_weighted_mean = scores['test_f1_weighted'].mean()
                            print("f1_score_weighted_mean = ", f1_score_weighted_mean)
                            #print("f1_score_weighted_std = ", scores['test_f1_weighted'].std())
                            print("*******************************************************")

                            if (max_recall_0_mean < recall_0_mean):
                                max_recall_0_mean = recall_0_mean
                                best_estimator_count = estimator

                            cv_row = str(estimator) + ", " + str(c) + ", " + str(depth) + ", " \
                                    + str(sample_split) + ", " + str(features) + ", " \
                                    + str(recall_0_mean) + ", " + str(recall_0_dev) + ", " \
                                    + str(recall_1_mean) + ", " + str(recall_1_dev) + ", " \
                                    + str(recall_2_mean) + ", " + str(recall_2_dev) + ", " \
                                    + str(recall_macro_mean) + ", " + str(recall_micro_mean) + ", " \
                                    + str(recall_weighted_mean) + ", " \
                                    + str(precision_0_mean) + ", " + str(precision_0_dev) + ", " \
                                    + str(precision_1_mean) + ", " + str(precision_1_dev) + ", " \
                                    + str(precision_2_mean) + ", " + str(precision_2_dev) + ", " \
                                    + str(precision_macro_mean) + ", " \
                                    + str(precision_micro_mean) + ", " + str(precision_weighted_mean) + ", " \
                                    + str(f1_0_mean) + ", " + str(f1_0_dev) + ", " \
                                    + str(f1_1_mean) + ", " + str(f1_1_dev) + ", " \
                                    + str(f1_2_mean) + ", " + str(f1_2_dev) + ", " \
                                    + str(f1_score_macro_mean) + ", " + str(f1_score_micro_mean) + ", " \
                                    + str(f1_score_weighted_mean) + '\n'
                            
                            self.__cv_file.write(cv_row)
        #print("Score", scores)

        #print("f1_score_samples = ", scores['test_f1_samples'])
        #print("precision_samples = ", scores['test_precision_samples'])
        #print("recall_samples = ", scores['test_recall_samples'])
        #print("accuracy = ", scores['test_accuracy'])

        return max_recall_0_mean, best_estimator_count
        
    def read_file(self):
        self.__db.read_db()

    def test_ignored_traces(self, merge_labels, estimators):
        all_training_problems = self.__db.get_all_training_problems()

        #ExtraTreesClassifier(n_estimators=10, criterion=’gini’, max_depth=None, \
        #min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, \
        #max_features=’auto’, max_leaf_nodes=None, min_impurity_decrease=0.0, \
        #min_impurity_split=None, bootstrap=False, oob_score=False, n_jobs=1, \
        #random_state=None, verbose=0, warm_start=False, class_weight=None)
        clf = ExtraTreesClassifier(n_estimators = estimators)
        clf.fit(all_training_problems.get_features_list(), all_training_problems.get_label_list())

        all_ignored_problems = self.__db.get_ignored_traces_problem_list()
        problem_features = all_ignored_problems.get_features_list()
        problem_labels = all_ignored_problems.get_label_list()
        predicted_labels = list()

        for problem in problem_features:
            #print("problem = ", problem)
            predicted_label = clf.predict([problem])
            predicted_labels.append(predicted_label[0])

        #print("predicted_labels = ", predicted_labels)
        #print("problem_labels = ", problem_labels) 

        if (merge_labels):
            print("%%%%%%%%%%%% Merging Labels %%%%%%%%%%")
            i = 0
            while i < len(predicted_labels):
                if (predicted_labels[i] == 2):
                    #print("Actual, Predicted ", problem_labels[i], predicted_labels[i])
                    predicted_labels[i] = 0
                    #print("## Actual, Predicted ", problem_labels[i], predicted_labels[i])

                if (problem_labels[i] == 2):
                    #print("Actual, Predicted ", problem_labels[i], predicted_labels[i])
                    problem_labels[i] = 0
                    #print("## Actual, Predicted ", problem_labels[i], predicted_labels[i])
            
                #if (problem_labels[i] == 0 and predicted_labels[i] == 2):
                #    print("Actual, Predicted ", problem_labels[i], predicted_labels[i])
                #    predicted_labels[i] =[0]
                #    print("## Actual, Predicted ", problem_labels[i], predicted_labels[i])

                #if (problem_labels[i] == 1 and predicted_labels[i] == 2):
                #    print("Actual, Predicted ", problem_labels[i], predicted_labels[i])
                #    predicted_labels[i] = [0]
                #    print("### Actual, Predicted ", problem_labels[i], predicted_labels[i])
                i = i + 1

        #print("predicted_labels = ", predicted_labels)
        #print("problem_labels = ", problem_labels) 
        
        print("Confusion Matrix = ", confusion_matrix(problem_labels, predicted_labels))
            
        recall = recall_score(problem_labels, predicted_labels, average=None)
        print("recall_score = ", recall)

        prec_score = precision_score(problem_labels, predicted_labels, average=None)
        print("precision_score = ", prec_score)
       
        f1 = f1_score(problem_labels, predicted_labels, average=None)
        print("f1_score = ", f1)
        
        recall_macro = recall_score(problem_labels, predicted_labels, average='macro')
        print("recall_score_macro = ", recall_macro)
                                
        recall_micro = recall_score(problem_labels, predicted_labels, average='micro')
        print("recall_score_micro = ", recall_micro)
                                
        precision_macro = precision_score(problem_labels, predicted_labels, average='macro')    
        print("precision_score_macro = ", precision_macro)
                                    
        precision_micro = precision_score(problem_labels, predicted_labels, average='micro')    
        print("precision_score_micro = ", precision_micro)
                                   
        f1_macro = f1_score(problem_labels, predicted_labels, average='macro') 
        print("f1_score_macro = ", f1_macro)
                                    
        f1_micro = f1_score(problem_labels, predicted_labels, average='micro')
        print("f1_score_micro = ", f1_micro)
        print("&&&&&&&&&&&&&&&&&&&&&&&&&")
        
        label_0_present = 0 in problem_labels
        label_1_present = 1 in problem_labels
        label_0_predicted = 0 in predicted_labels
        label_1_predicted = 1 in predicted_labels

        metrics = ['inf', 'inf', 'inf', 'inf']
        
        if (label_0_present):
            metrics[0] = recall[0]

        if (label_0_present and label_1_present):
            metrics[1] = recall[1]
        elif (label_1_present):
            metrics[1] = recall[0]

        if (label_0_predicted):
            metrics[2] = prec_score[0]

        if (label_0_predicted and label_1_predicted):
            metrics[3] = prec_score[1]
        elif (label_1_predicted):
            metrics[3] = prec_score[0]

        return float(metrics[0]), float(metrics[1]), float(metrics[2]), float(metrics[3])                       

    def do_cv(self):
        #self.__file = open(self.__debug_file_name, "w")
        #self.__cv_file = open(self.__cv_file_name, "w")

        self.__db.read_db()

        #if (self.__mode == 1):
        #    max_recall_0_mean, estimator = self.cv_per_trace()
        #else:
        max_recall_0_mean, estimator = self.cv_per_problem()
        
        #self.__file.close()
        #self.__cv_file.close()

        return max_recall_0_mean, estimator
        
    def get_features_list(self):
        return self.__features_list


# First argument - input csv file
# Second argument - 1/0 - CV based on trace/problem
# Third argument - skip trace

def test(data_file, fold, iterations, ignored_traces, compare_past_workload_for_similarity, merge_labels = True):
    #ignored_traces =  ['/home/ashutosp/Sandbox/Experiments/Traces/wc_day_38_fixed_105_4.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_16_fixed_105_75.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_63_fixed_105_25.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_34_fixed_105_035.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_91_fixed_105_013.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_27_fixed_105_23.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_29_fixed_105_0175.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_84_fixed_105_3.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_19_fixed_105_14.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_52_fixed_105_10.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_25_fixed_105_2.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_31_fixed_105_25.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_9_fixed_105_06.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_49_fixed_105_24.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_12_fixed_105_12.delta', '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_22_fixed_105_012.delta']
    #ignored_traces = ['/home/ashutosp/Sandbox/Experiments/Traces/wc_day_42_fixed_105_5.delta 16']
    #ignored_traces = ['/home/ashutosp/Sandbox/Experiments/Traces/wc_day_72_fixed_105_10.delta']
    cv = CrossValidate(data_file, fold, iterations, ignored_traces, compare_past_workload_for_similarity)
    cv.read_file()
    return cv.test_ignored_traces(merge_labels, 93)

def main():
    # print command line arguments
    #for arg in sys.argv[1:]:
    #    print (arg)

    print (sys.argv[0])
    print (sys.argv[1])
    print (sys.argv[2])
    print (sys.argv[3])
    test_mode = False
    merge_labels = False
    compare_past_workload_for_similarity = 0

    if (len(sys.argv) > 4):
        compare_past_workload_for_similarity = int(sys.argv[4])

    if (len(sys.argv) > 5):
        merge_labels = int(sys.argv[5])

    print("compare_past_workload_for_similarity = ", compare_past_workload_for_similarity)
    print("merge_labels = ", merge_labels)
    print("Parsing to get traces")
    db_wrapper = DBWrapper(sys.argv[1], list())
    db_wrapper.read_db()
    all_traces = db_wrapper.get_all_traces()
    #print("all_traces = ", all_traces)
    trace_list = list()

    for trace in all_traces:
        #if (trace != '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_22_fixed_105_012.delta' \
        #        and trace != '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_23_fixed_105_12.delta' \
        #        and trace != '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_62_fixed_105_50.delta' \
        #        and trace != '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_69_fixed_105_41.delta' \
        #        and trace != '/home/ashutosp/Sandbox/Experiments/Traces/wc_day_73_fixed_105_60.delta'):
        trace_list.append(trace)

    if (test_mode):
        total_traces = len(trace_list)
        #print("total_traces = ", total_traces)
        fold_size = int(total_traces/int(sys.argv[2]))
        print("fold_size = ", fold_size)
        iteration = 0
        recall_0_avg = 0
        recall_1_avg = 0
        precision_0_avg = 0
        precision_1_avg = 0

        while (iteration < int(sys.argv[3])):
            test_trace_indices = get_random_numbers(0, total_traces - 1, fold_size)
            #print("test_trace_indices = ", test_trace_indices)
            ignored_traces = list()

            for index in test_trace_indices:
                ignored_traces.append(trace_list[index])
    
            #print("ignored_traces = ", ignored_traces)
            print("@@@@@@@@@@@@@@ iteration = ", iteration, "@@@@@@@@@@@@@@@@@@@@")
            recall_0, recall_1, precision_0, precision_1 = test(sys.argv[1], sys.argv[2], \
                    sys.argv[3], ignored_traces, compare_past_workload_for_similarity, merge_labels)
            print("recall_0, recall_1, precision_0, precision_1", recall_0, recall_1, precision_0, precision_1)
            recall_0_avg = recall_0_avg + recall_0
            recall_1_avg = recall_1_avg + recall_1
            precision_0_avg = precision_0_avg + precision_0
            precision_1_avg = precision_1_avg + precision_1
            print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
            
            iteration = iteration + 1

        recall_0_avg = recall_0_avg/(int(sys.argv[3]))
        recall_1_avg = recall_1_avg/(int(sys.argv[3]))
        precision_0_avg = precision_0_avg/(int(sys.argv[3]))
        precision_1_avg = precision_1_avg/(int(sys.argv[3]))
        print("recall_0 average = ", recall_0_avg)
        print("recall_1 average = ", recall_1_avg)
        print("precision_0 average = ", precision_0_avg)
        print("precision_1 average = ", precision_1_avg)
    else:
        ignored_traces = list()
        trace_debug_file_name = sys.argv[2] + "_trace_log.csv"
        debug_trace_file = open(trace_debug_file_name, "w")
        debug_trace_file.write("Trace, estimators, recall_score_0, recall_score_1, \
                                precision_score_0, precision_score_1, max_recall_0_mean\n")
        file_name_index = 0
       
        for trace in trace_list:
            debug_file_name = sys.argv[2] + "_" + str(file_name_index) + "_ExtraTreesClassifier_log.csv"
            cv_file_name = sys.argv[2] + "_" + str(file_name_index) + "_cv_ExtraTreesClassifier_log.csv"
            print("============= Trace, Index ", trace, file_name_index)
            debug_file = open(debug_file_name, "w")
            cv_file = open(cv_file_name, "w")

            file_name_index = file_name_index + 1

            ignored_traces = [trace]
            cv = CrossValidate(sys.argv[1], sys.argv[2], sys.argv[3], ignored_traces, \
                    compare_past_workload_for_similarity, debug_file, cv_file)
            max_recall_0_mean, estimators = cv.do_cv()
            print("max_recall_0_mean, estimators", max_recall_0_mean, estimators)
            recall_0, recall_1, prec_score_0, prec_score_1 = cv.test_ignored_traces(merge_labels, estimators)
            row = str(trace) + "," + str(estimators) + "," + str(recall_0) + ", " + str(recall_1) + ","\
                  + str(prec_score_0) + ", " + str(prec_score_1) + "," + str(max_recall_0_mean) + '\n'
            debug_trace_file.write(row)
            print("recall_0, recall_1, prec_score_0, prec_score_1", recall_0, recall_1, prec_score_0, prec_score_1)
            debug_file.close()
            cv_file.close()

        debug_trace_file.close()
    
    #print("Final f1_score = ", accuracy)
    
    

#def main():
#    knn = None
#    knn = KnnClassifier("features2.csv", 20)
    #knn.populate_db()
    #knn.fit()
#    knn.train()
    #t1 = current_time()
    #start = time.time()
#    result = knn.predict([[1,2,3,4,5,6,7,8,9, 10, 11, float(12), float(13), \
#                        float(14), float(15), float(16), float(17), float(18), float(19), \
#                        float(20), float(21), float(22), float(23), \
#                        float(24), float(25), float(132.222),  float(141.602),  \
#                        float(147.459),  float(153.029),  float(158.291),  float(163.23)]])
    #end = time.time()
    #print(end - start)
    #print(current_time() - t1)

#    return result
            
if __name__ == "__main__":
    sys.exit(main())
