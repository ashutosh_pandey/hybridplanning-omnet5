import sys
from ResultDB import ResultDB
from TraceData import TraceData
from ReadResult import ReadResult
from DBWriter import DBWriter
from statistics import stdev

def dump_aggregate_utility(result_db):
    writer = open("AggregateUtility.csv", "w+")
    header_str = "Trace, Fast, Slow, SlowInstant, Constraint_HP, ML0, ML1\n"
    writer.write(header_str)

    days = result_db.get_days()
    days.sort()
    for day_path in days:
        day_data = result_db.get_trace_data(day_path)
        elements = day_path.split('/')
        day = elements[len(elements) - 1]
        writer.write(day + ", ")
        writer.write(str(day_data.get_aggregate_fast_utility()) + ", ")
        writer.write(str(day_data.get_aggregate_slow_utility()) + ", ")
        writer.write(str(day_data.get_aggregate_slow_instant_utility()) + ", ")
        writer.write(str(day_data.get_aggregate_hybrid_utility()) + ", ")
        writer.write(str(day_data.get_aggregate_ibl0_utility()) + ", ")
        writer.write(str(day_data.get_aggregate_ibl1_utility()) + "\n")
         
    writer.close()

def main():
    print (sys.argv[1])
    print (sys.argv[2])
    
    fast = True
    slow_instant = True
    slow = True
    hybrid = True
    hybrid_ibl_0 = True
    hybrid_ibl_1 = True

    read_result = ReadResult(sys.argv[1], fast, slow, hybrid, hybrid_ibl_0, hybrid_ibl_1, slow_instant)
    result_db = read_result.get_result_db()

    dump_aggregate_utility(result_db)

    days = result_db.get_days()
    days.sort()
    total_pass = 0
    total_failed = 0
    total_data = 0

    pass_days = list()
    fail_days = list()

    hybrid_better = list()
    hybrid_not_better = list()
    hybrid_same = list()

    hybrid_wins = list()
    hybrid_losses = list()

    hybrid_wins_ibl0 = list()
    hybrid_wins_ibl0_hybrid_greater = list()
    hybrid_wins_ibl0_hybrid_lesser = list()
    hybrid_wins_hybrid = list()
    hybrid_wins_hybrid_ibl0_greater = list()
    hybrid_wins_hybrid_ibl0_lesser = list()
    hybrid_wins_same = list()

    hybrid_losses_slow = list()
    hybrid_losses_slow_ibl0_wins = list()
    hybrid_losses_slow_hybrid_wins = list()
    hybrid_losses_fast = list()
    hybrid_losses_fast_ibl0_wins = list()
    hybrid_losses_fast_hybrid_wins = list()
    hybrid_looses_same = list()

    special_cases = list()

    ibl1_wins_ibl0 = list()
    ibl0_wins_ibl1 = list()

    ibl0_wins = list()
    ibl0_same = list()
    ibl0_looses = list()
    ibl1_wins = list()
    ibl1_same = list()
    ibl1_looses = list()
    
    slow_wins_fast_ibl0_wins = list()
    slow_wins_fast_ibl0_looses = list()
    slow_looses_fast_ibl0_wins = list()
    slow_looses_fast_ibl0_looses = list()

    slow_wins_fast_ibl1_wins = list()
    slow_wins_fast_ibl1_looses = list()
    slow_looses_fast_ibl1_wins = list()
    slow_looses_fast_ibl1_looses = list()

    slow_wins_fast_hybrid_wins = list()
    slow_wins_fast_hybrid_looses = list()
    slow_looses_fast_hybrid_wins = list()
    slow_looses_fast_hybrid_looses = list()

    slow_instant_wins_fast = list()
    slow_instant_same_fast = list()
    slow_instant_looses_fast = list()
    #slow_wins_fast_same_wins = list()
    #slow_wins_fast_same_looses = list()
    #slow_looses_fast_same_wins = list()
    #slow_looses_fast_same_looses = list()

    ibl0_less_than_slow_fast = list()
    ibl1_less_than_slow_fast = list()
    hybrid_less_than_slow_fast = list()

    ibl0_hybrid_wins = list()
    hybrid_hybrid_wins = list()
    ibl1_hybrid_wins = list()

    constraint_looses_slow = list()
    constraint_looses_fast = list()
    total_fast = list()
    total_slow = list()
    total_slow_instant = list()
    total_hybrid = list()
    total_ibl0 = list()
    total_ibl1 = list()

    # Hybrid Passed
        # ibl0 is best
            # hybrid is smaller than slow or fast
        # hybrid is best
            # ibl0 is smaller than slow or fast
        # ibl0 == hybrid > fast and slow
    # Hybrid Failed
        # fast is best
            # ibl0 > hybrid
            # hybrid > ibl0
            # hybrid == ibl0
        # slow is best
            # ibl0 > hybrid
            # hybrid > ibl0
            # hybrid == ibl0
    for day_path in days:
        total_data = total_data + 1
        day_data = result_db.get_trace_data(day_path)
        elements = day_path.split('/')
        day = elements[len(elements) - 1]
        #print ("day = ", day)

        #print(len(day_data.get_ibl0_utility()))
        #print(len(day_data.get_fast_utility()))
        #print(len(day_data.get_slow_utility()))
        #print(len(day_data.get_hybrid_utility()))

        if (len(day_data.get_ibl0_utility()) != 0 \
            and (len(day_data.get_fast_utility()) != 0) \
            and (len(day_data.get_slow_utility()) != 0) \
            and (len(day_data.get_slow_instant_utility()) != 0) \
            and (len(day_data.get_hybrid_utility()) != 0) \
            and (len(day_data.get_ibl1_utility()) != 0)):

            if (day_data.get_aggregate_slow_instant_utility() > day_data.get_aggregate_fast_utility()):
                slow_instant_wins_fast.append(day)
            elif (day_data.get_aggregate_slow_instant_utility() < day_data.get_aggregate_fast_utility()):
                slow_instant_looses_fast.append(day)
            else:
                slow_instant_same_fast.append(day)
            
            if (day_data.get_aggregate_ibl0_utility() > day_data.get_aggregate_hybrid_utility()):
                ibl0_wins.append(day)
            elif (day_data.get_aggregate_ibl0_utility() == day_data.get_aggregate_hybrid_utility()):
                ibl0_same.append(day)
            else:
                ibl0_looses.append(day)
            
            if (day_data.get_aggregate_hybrid_utility() <  day_data.get_aggregate_fast_utility()):
                constraint_looses_fast.append(day)
            
            if (day_data.get_aggregate_hybrid_utility() <  day_data.get_aggregate_slow_utility()):
                constraint_looses_slow.append(day)
                
            if (day_data.get_aggregate_slow_utility() >= day_data.get_aggregate_fast_utility()):
                if (day_data.get_aggregate_ibl0_utility() >= day_data.get_aggregate_fast_utility() \
                        and day_data.get_aggregate_ibl0_utility() >= day_data.get_aggregate_slow_utility()):
                    slow_wins_fast_ibl0_wins.append(day)
                else:
                    slow_wins_fast_ibl0_looses.append(day)
            else:
                if (day_data.get_aggregate_ibl0_utility() >= day_data.get_aggregate_fast_utility() \
                        and day_data.get_aggregate_ibl0_utility() >= day_data.get_aggregate_slow_utility()):
                    slow_looses_fast_ibl0_wins.append(day)
                else:
                    slow_looses_fast_ibl0_looses.append(day)

            if (day_data.get_aggregate_slow_utility() >= day_data.get_aggregate_fast_utility()):
                if (day_data.get_aggregate_ibl1_utility() >= day_data.get_aggregate_fast_utility() \
                        and day_data.get_aggregate_ibl1_utility() >= day_data.get_aggregate_slow_utility()):
                    slow_wins_fast_ibl1_wins.append(day)
                else:
                    slow_wins_fast_ibl1_looses.append(day)
            else:
                if (day_data.get_aggregate_ibl1_utility() >= day_data.get_aggregate_fast_utility() \
                        and day_data.get_aggregate_ibl1_utility() >= day_data.get_aggregate_slow_utility()):
                    slow_looses_fast_ibl1_wins.append(day)
                else:
                    slow_looses_fast_ibl1_looses.append(day)

            if (day_data.get_aggregate_slow_utility() >= day_data.get_aggregate_fast_utility()):
                if (day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_fast_utility() \
                        and day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_slow_utility()):
                    slow_wins_fast_hybrid_wins.append(day)
                else:
                    slow_wins_fast_hybrid_looses.append(day)
            else:
                if (day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_fast_utility() \
                        and day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_slow_utility()):
                    slow_looses_fast_hybrid_wins.append(day)
                else:
                    slow_looses_fast_hybrid_looses.append(day)

            if (day_data.get_aggregate_ibl0_utility() < day_data.get_aggregate_fast_utility() \
                        and day_data.get_aggregate_ibl0_utility() < day_data.get_aggregate_slow_utility()):
                ibl0_less_than_slow_fast.append(day)

            if (day_data.get_aggregate_hybrid_utility() < day_data.get_aggregate_fast_utility() \
                        and day_data.get_aggregate_hybrid_utility() < day_data.get_aggregate_slow_utility()):
                hybrid_less_than_slow_fast.append(day)

            if (day_data.get_aggregate_ibl1_utility() < day_data.get_aggregate_fast_utility() \
                        and day_data.get_aggregate_ibl1_utility() < day_data.get_aggregate_slow_utility()):
                ibl1_less_than_slow_fast.append(day)

            
            if (day_data.get_aggregate_ibl0_utility() >= day_data.get_aggregate_fast_utility() \
                        and day_data.get_aggregate_ibl0_utility() >= day_data.get_aggregate_slow_utility()):
                ibl0_hybrid_wins.append(day)

            if (day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_fast_utility() \
                        and day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_slow_utility()):
                hybrid_hybrid_wins.append(day)

            if (day_data.get_aggregate_ibl1_utility() >= day_data.get_aggregate_fast_utility() \
                        and day_data.get_aggregate_ibl1_utility() >= day_data.get_aggregate_slow_utility()):
                ibl1_hybrid_wins.append(day)

            total_fast.append(day_data.get_aggregate_fast_utility())
            total_slow.append(day_data.get_aggregate_slow_utility())
            total_slow_instant.append(day_data.get_aggregate_slow_instant_utility())
            total_hybrid.append(day_data.get_aggregate_hybrid_utility())
            total_ibl0.append(day_data.get_aggregate_ibl0_utility())
            total_ibl1.append(day_data.get_aggregate_ibl1_utility())

            if (day_data.get_aggregate_ibl0_utility() >= day_data.get_aggregate_fast_utility() \
                and day_data.get_aggregate_ibl0_utility() >= day_data.get_aggregate_slow_utility() \
                and day_data.get_aggregate_ibl0_utility() > day_data.get_aggregate_hybrid_utility()):
                hybrid_wins_ibl0.append(day)

                if (day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_fast_utility() \
                    and day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_slow_utility()):
                    hybrid_wins_ibl0_hybrid_greater.append(day)
                else:
                    hybrid_wins_ibl0_hybrid_lesser.append(day)

            elif (day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_fast_utility() \
                and day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_slow_utility() \
                and day_data.get_aggregate_hybrid_utility() > day_data.get_aggregate_ibl0_utility()):
                hybrid_wins_hybrid.append(day)

                if (day_data.get_aggregate_ibl0_utility() >= day_data.get_aggregate_fast_utility() \
                    and day_data.get_aggregate_ibl0_utility() >= day_data.get_aggregate_slow_utility()):
                    hybrid_wins_hybrid_ibl0_greater.append(day)
                else:
                    hybrid_wins_hybrid_ibl0_lesser.append(day)
                
            elif (day_data.get_aggregate_ibl0_utility() >= day_data.get_aggregate_fast_utility() \
                and day_data.get_aggregate_ibl0_utility() >= day_data.get_aggregate_slow_utility() \
                and day_data.get_aggregate_ibl0_utility() == day_data.get_aggregate_hybrid_utility()):
                hybrid_wins_same.append(day)
            elif (day_data.get_aggregate_fast_utility() > day_data.get_aggregate_slow_utility()):
                hybrid_losses_fast.append(day)
                if (day_data.get_aggregate_ibl0_utility() > day_data.get_aggregate_hybrid_utility()):
                    hybrid_losses_fast_ibl0_wins.append(day)
                elif day_data.get_aggregate_ibl0_utility() < day_data.get_aggregate_hybrid_utility():
                    hybrid_losses_fast_hybrid_wins.append(day)
                else:
                    hybrid_looses_same.append(day)
            elif (day_data.get_aggregate_fast_utility() < day_data.get_aggregate_slow_utility()):
                hybrid_losses_slow.append(day)
                if (day_data.get_aggregate_ibl0_utility() > day_data.get_aggregate_hybrid_utility()):
                    hybrid_losses_slow_ibl0_wins.append(day)
                elif day_data.get_aggregate_ibl0_utility() < day_data.get_aggregate_hybrid_utility():
                    hybrid_losses_slow_hybrid_wins.append(day)
                else:
                    hybrid_looses_same.append(day)
            else:
                special_cases.append(day)    

        assert(total_data == (len(hybrid_wins_ibl0) + len(hybrid_wins_hybrid) \
                + len(hybrid_wins_same) + len(hybrid_losses_fast) + len(hybrid_losses_slow) \
                + len(special_cases)))

    print("Total Data = ", total_data)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_ibl0 = ", len(hybrid_wins_ibl0), " ### ", hybrid_wins_ibl0)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_ibl0_hybrid_greater = ", len(hybrid_wins_ibl0_hybrid_greater), \
                " ### ", hybrid_wins_ibl0_hybrid_greater)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_ibl0_hybrid_lesser = ", len(hybrid_wins_ibl0_hybrid_lesser), \
                " ### ", hybrid_wins_ibl0_hybrid_lesser)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_hybrid = ", len(hybrid_wins_hybrid), " ### ",  hybrid_wins_hybrid)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_hybrid_ibl0_greater = ", len(hybrid_wins_hybrid_ibl0_greater), \
                " ### ",  hybrid_wins_hybrid_ibl0_greater)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_hybrid_ibl0_lesser = ", len(hybrid_wins_hybrid_ibl0_lesser), \
                " ### ",  hybrid_wins_hybrid_ibl0_lesser)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_same = ", len(hybrid_wins_same), " ### ",  hybrid_wins_same)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

    print("hybrid_looses_slow = ", len(hybrid_losses_slow), " ### ",  hybrid_losses_slow)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_losses_slow_ibl0_wins = ", len(hybrid_losses_slow_ibl0_wins), " ### ",  \
                hybrid_losses_slow_ibl0_wins)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_losses_slow_hybrid_wins = ", len(hybrid_losses_slow_hybrid_wins), \
                " ### ",  hybrid_losses_slow_hybrid_wins)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_losses_fast = ", len(hybrid_losses_fast), " ### ",  hybrid_losses_fast)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_losses_fast_ibl0_wins = ", len(hybrid_losses_fast_ibl0_wins), \
                " ### ",  hybrid_losses_fast_ibl0_wins)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_losses_fast_hybrid_wins = ", len(hybrid_losses_fast_hybrid_wins), \
                " ### ",  hybrid_losses_fast_hybrid_wins)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_looses_same = ", len(hybrid_looses_same), " ### ",  hybrid_looses_same)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

    print("special_cases = ", len(special_cases), " ### ",  special_cases)

    print("slow_wins_fast_ibl0_wins = ", len(slow_wins_fast_ibl0_wins), slow_wins_fast_ibl0_wins)
    print("slow_wins_fast_ibl0_looses = ", len(slow_wins_fast_ibl0_looses), slow_wins_fast_ibl0_looses)
    print("slow_looses_fast_ibl0_wins = ", len(slow_looses_fast_ibl0_wins), slow_looses_fast_ibl0_wins)
    print("slow_looses_fast_ibl0_looses = ", len(slow_looses_fast_ibl0_looses), slow_looses_fast_ibl0_looses)
    print("slow_wins_fast_ibl1_wins = ", len(slow_wins_fast_ibl1_wins), slow_wins_fast_ibl1_wins)
    print("slow_wins_fast_ibl1_looses = ", len(slow_wins_fast_ibl1_looses), slow_wins_fast_ibl1_looses)
    print("slow_looses_fast_ibl1_wins = ", len(slow_looses_fast_ibl1_wins), slow_looses_fast_ibl1_wins)
    print("slow_looses_fast_ibl1_looses = ", len(slow_looses_fast_ibl1_looses), slow_looses_fast_ibl1_looses)
    print("slow_wins_fast_hybrid_wins = ", len(slow_wins_fast_hybrid_wins), slow_wins_fast_hybrid_wins)
    print("slow_wins_fast_hybrid_looses = ", len(slow_wins_fast_hybrid_looses), slow_wins_fast_hybrid_looses)
    print("slow_looses_fast_hybrid_wins = ", len(slow_looses_fast_hybrid_wins), slow_looses_fast_hybrid_wins)
    print("slow_looses_fast_hybrid_looses = ", len(slow_looses_fast_hybrid_looses), slow_looses_fast_hybrid_looses)
    print("ibl0_less_than_slow_fast = ", len(ibl0_less_than_slow_fast), ibl0_less_than_slow_fast)
    print("hybrid_less_than_slow_fast = ", len(hybrid_less_than_slow_fast), hybrid_less_than_slow_fast)
    print("ibl1_less_than_slow_fast = ", len(ibl1_less_than_slow_fast), ibl1_less_than_slow_fast)
 
    print("ibl0_hybrid_wins = ", len(ibl0_hybrid_wins), ibl0_hybrid_wins)
    print("hybrid_hybrid_wins = ", len(hybrid_hybrid_wins), hybrid_hybrid_wins)
    print("ibl1_hybrid_wins = ", len(ibl1_hybrid_wins), ibl1_hybrid_wins)

    print("constraint_looses_slow = ", len(constraint_looses_slow), constraint_looses_slow)
    print("constraint_looses_fast = ", len(constraint_looses_fast), constraint_looses_fast)

    print("slow_instant_wins_fast = ", len(slow_instant_wins_fast), slow_instant_wins_fast)
    print("slow_instant_same_fast = ", len(slow_instant_same_fast), slow_instant_same_fast)
    print("slow_instant_looses_fast = ", len(slow_instant_looses_fast), slow_instant_looses_fast)
        #if (len(day_data.get_hybrid_ibl_0_utility()) != 0 and (len(day_data.get_fast_utility()) != 0)):
        #    total_data = total_data + 1
        #    if (day_data.get_aggregate_fast_utility() > day_data.get_aggregate_hybrid_ibl_0_utility()):
        #        total_failed = total_failed + 1
        #        fail_days.append(elements[len(elements) - 1])
                #print("day = ", day)
                #print("FAILED")
        #    else:
        #        total_pass = total_pass + 1
        #        pass_days.append(elements[len(elements) - 1])
                #print("day = ", day)
                #print("PASSED")

        #if (len(day_data.get_hybrid_ibl_0_utility()) != 0 and (len(day_data.get_hybrid_utility()) != 0)):
        #    if (day_data.get_aggregate_hybrid_ibl_0_utility() > day_data.get_aggregate_hybrid_utility()):
        #        hybrid_not_better.append(elements[len(elements) - 1])
        #    elif (day_data.get_aggregate_hybrid_ibl_0_utility() == day_data.get_aggregate_hybrid_utility()):
        #        hybrid_same.append(elements[len(elements) - 1])
        #    else:
                #if (day == "/home/ashutosp/Dropbox/regression/Day6"):
                #    print(day_data.get_hybrid_ibl_0_utility())
                #    print(day_data.get_hybrid_utility())
        #        hybrid_better.append(elements[len(elements) - 1])
               
    #print("Total Data = ", total_data)
    #print("Total Passed = ", total_pass)
    #print("Total Failed = ", total_failed)

    #print("Passed = ", pass_days)
    #print("Fail = ", fail_days)

    #print("hybrid_not_better = ", len(hybrid_not_better), hybrid_not_better)
    #print("hybrid_same = ", len(hybrid_same), hybrid_same)
    #print("hybrid_better = ", len(hybrid_better), hybrid_better)

    #print(len(hybrid_not_better))
    #print(len(hybrid_same))
    #print(len(hybrid_better))
    hybrid_better = []
    hybrid_not_better = []
    hybrid_same = []

    hybrid_wins = []
    hybrid_losses = []

    hybrid_wins_ibl1 = []
    hybrid_wins_ibl1_hybrid_greater = []
    hybrid_wins_ibl1_hybrid_lesser = []
    hybrid_wins_hybrid = []
    hybrid_wins_hybrid_ibl1_greater = []
    hybrid_wins_hybrid_ibl1_lesser = []
    hybrid_wins_same = []

    hybrid_losses_slow = []
    hybrid_losses_slow_ibl1_wins = []
    hybrid_losses_slow_hybrid_wins = []
    hybrid_losses_fast = []
    hybrid_losses_fast_ibl1_wins = []
    hybrid_losses_fast_hybrid_wins = []
    hybrid_looses_same = []
    
    special_cases = []

    for day_path in days:
        total_data = total_data + 1
        day_data = result_db.get_trace_data(day_path)
        elements = day_path.split('/')
        day = elements[len(elements) - 1]
        #print ("day = ", day)
        
        if (len(day_data.get_ibl0_utility()) != 0 \
            and (len(day_data.get_fast_utility()) != 0) \
            and (len(day_data.get_slow_utility()) != 0) \
            and (len(day_data.get_hybrid_utility()) != 0) \
            and (len(day_data.get_ibl1_utility()) != 0)):
            
            if (day_data.get_aggregate_ibl1_utility() > day_data.get_aggregate_hybrid_utility()):
                ibl1_wins.append(day)
            elif (day_data.get_aggregate_ibl1_utility() == day_data.get_aggregate_hybrid_utility()):
                ibl1_same.append(day)
            else:
                ibl1_looses.append(day)
        
            if (day_data.get_aggregate_ibl1_utility() >= day_data.get_aggregate_fast_utility() \
                and day_data.get_aggregate_ibl1_utility() >= day_data.get_aggregate_slow_utility() \
                and day_data.get_aggregate_ibl1_utility() > day_data.get_aggregate_hybrid_utility()):
                hybrid_wins_ibl1.append(day)

                if (day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_fast_utility() \
                    and day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_slow_utility()):
                    hybrid_wins_ibl1_hybrid_greater.append(day)
                else:
                    hybrid_wins_ibl1_hybrid_lesser.append(day)
            elif (day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_fast_utility() \
                and day_data.get_aggregate_hybrid_utility() >= day_data.get_aggregate_slow_utility() \
                and day_data.get_aggregate_hybrid_utility() > day_data.get_aggregate_ibl1_utility()):
                hybrid_wins_hybrid.append(day)

                if (day_data.get_aggregate_ibl1_utility() >= day_data.get_aggregate_fast_utility() \
                    and day_data.get_aggregate_ibl1_utility() >= day_data.get_aggregate_slow_utility()):
                    hybrid_wins_hybrid_ibl1_greater.append(day)
                else:
                    hybrid_wins_hybrid_ibl1_lesser.append(day)
            elif (day_data.get_aggregate_ibl1_utility() >= day_data.get_aggregate_fast_utility() \
                and day_data.get_aggregate_ibl1_utility() >= day_data.get_aggregate_slow_utility() \
                and day_data.get_aggregate_ibl1_utility() == day_data.get_aggregate_hybrid_utility()):
                hybrid_wins_same.append(day)
            elif (day_data.get_aggregate_fast_utility() > day_data.get_aggregate_slow_utility()):
                hybrid_losses_fast.append(day)
                if (day_data.get_aggregate_ibl1_utility() > day_data.get_aggregate_hybrid_utility()):
                    hybrid_losses_fast_ibl1_wins.append(day)
                elif day_data.get_aggregate_ibl1_utility() < day_data.get_aggregate_hybrid_utility():
                    hybrid_losses_fast_hybrid_wins.append(day)
                else:
                    hybrid_looses_same.append(day)
            elif (day_data.get_aggregate_fast_utility() < day_data.get_aggregate_slow_utility()):
                hybrid_losses_slow.append(day)
                if (day_data.get_aggregate_ibl1_utility() > day_data.get_aggregate_hybrid_utility()):
                    hybrid_losses_slow_ibl1_wins.append(day)
                elif day_data.get_aggregate_ibl1_utility() < day_data.get_aggregate_hybrid_utility():
                    hybrid_losses_slow_hybrid_wins.append(day)
                else:
                    hybrid_looses_same.append(day)
            else:
                special_cases.append(day)    

        #assert(total_data == (len(hybrid_wins_ibl0) + len(hybrid_wins_hybrid) \
        #        + len(hybrid_wins_same) + len(hybrid_losses_fast) + len(hybrid_looses_slow) \
        #        + len(special_cases)))

    #print("Total Data = ", total_data)
    print("******************************************************************")
    print("******************************************************************")
    print("******************************************************************")
    print("******************************************************************")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_ibl1 = ", len(hybrid_wins_ibl1), " ### ", hybrid_wins_ibl1)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_ibl1_hybrid_greater = ", len(hybrid_wins_ibl1_hybrid_greater), \
                " ### ", hybrid_wins_ibl1_hybrid_greater)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_ibl1_hybrid_lesser = ", len(hybrid_wins_ibl1_hybrid_lesser), \
                " ### ", hybrid_wins_ibl1_hybrid_lesser)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_hybrid = ", len(hybrid_wins_hybrid), " ### ",  hybrid_wins_hybrid)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_hybrid_ibl1_greater = ", len(hybrid_wins_hybrid_ibl1_greater), \
                " ### ",  hybrid_wins_hybrid_ibl1_greater)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_hybrid_ibl1_lesser = ", len(hybrid_wins_hybrid_ibl1_lesser), \
                " ### ",  hybrid_wins_hybrid_ibl1_lesser)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_wins_same = ", len(hybrid_wins_same), " ### ",  hybrid_wins_same)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

    print("hybrid_looses_slow = ", len(hybrid_losses_slow), " ### ",  hybrid_losses_slow)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_losses_slow_ibl1_wins = ", len(hybrid_losses_slow_ibl1_wins), " ### ",  \
                hybrid_losses_slow_ibl1_wins)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_losses_slow_hybrid_wins = ", len(hybrid_losses_slow_hybrid_wins), \
                " ### ",  hybrid_losses_slow_hybrid_wins)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_losses_fast = ", len(hybrid_losses_fast), " ### ",  hybrid_losses_fast)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_losses_fast_ibl1_wins = ", len(hybrid_losses_fast_ibl1_wins), \
                " ### ",  hybrid_losses_fast_ibl1_wins)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_losses_fast_hybrid_wins = ", len(hybrid_losses_fast_hybrid_wins), \
                " ### ",  hybrid_losses_fast_hybrid_wins)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("hybrid_looses_same = ", len(hybrid_looses_same), " ### ",  hybrid_looses_same)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

    print("special_cases = ", len(special_cases), " ### ",  special_cases)

    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("ibl0_wins = ", len(ibl0_wins), " ### ",  ibl0_wins)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("ibl0_same = ", len(ibl0_same), " ### ",  ibl0_same)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("ibl0_looses = ", len(ibl0_looses), " ### ",  ibl0_looses)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("ibl1_wins = ", len(ibl1_wins), " ### ",  ibl1_wins)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("ibl1_same = ", len(ibl1_same), " ### ",  ibl1_same)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("ibl1_looses = ", len(ibl1_looses), " ### ",  ibl1_looses)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

    print("Total_fast = ", sum(total_fast))
    print("stdev_fast = ", stdev(total_fast))
    print("Total_slow = ", sum(total_slow))
    print("stdev_slow = ", stdev(total_slow))
    print("Total_slow_instant = ", sum(total_slow_instant))
    print("stdev_slow_instant = ", stdev(total_slow_instant))
    print("Total_hybrid = ", sum(total_hybrid))
    print("stdev_hybrid = ", stdev(total_hybrid))
    print("Total_ibl0 = ", sum(total_ibl0))
    print("stdev_ibl0 = ", stdev(total_ibl0))
    print("Total_ibl1 = ", sum(total_ibl1))
    print("stdev_ibl1 = ", stdev(total_ibl1))

    db_writer = DBWriter(sys.argv[2], result_db)
    db_writer.write()
    #day6_data = result_db.get_trace_data(sys.argv[1] + "Day6")
    #day6_hybrid_ibl0 = day6_data.get_aggregate_hybrid_ibl_0_utility()
    #print("day6_hybrid_ibl0 = ", day6_hybrid_ibl0)

if __name__ == "__main__":
    sys.exit(main())
