import sys
from ResultDB import ResultDB
from TraceData import TraceData

class DBWriter:
    def __init__(self, output_file, result_db):
        self.__output_file = output_file
        self.__result_db = result_db
        self.__writer = open(self.__output_file, "w+")

    def __write_header(self, days):
        #print("Writing Header")
        for path in days:
            path_elements = path.split('/')
            day = path_elements[len(path_elements) - 1]
            header_str = ",," + day + ",,,,,,"
            self.__writer.write(header_str)
         
        self.__writer.write("\n")
        
        for path in days:
            header_str = "Fast, Slow, SlowInstant, Hybrid, Hybrid_IBL0, Hybrid_IBL1, , , ,"
            self.__writer.write(header_str)
         
        self.__writer.write("\n")
        
    def write(self):
        days = self.__result_db.get_days()
        days.sort()

        self.__write_header(days)

        index = 0

        while (index < 90):
            for day in days:
                trace_data = self.__result_db.get_trace_data(day)

                fast_utility = trace_data.get_fast_utility()
                slow_utility = trace_data.get_slow_utility()
                slow_instant_utility = trace_data.get_slow_instant_utility()
                hybrid_utility = trace_data.get_hybrid_utility()
                hybrid_ibl0_utility = trace_data.get_ibl0_utility()
                hybrid_ibl1_utility = trace_data.get_ibl1_utility()

                day_data_str = ""

                #print("index = ", index)
                if (len(fast_utility) > index):
                    day_data_str = day_data_str + str(fast_utility[index]) + ","
                else:
                    day_data_str = day_data_str + ","

                if (len(slow_utility) > index):
                    day_data_str = day_data_str + str(slow_utility[index]) + ","
                else:
                    day_data_str = day_data_str + ","

                if (len(slow_instant_utility) > index):
                    day_data_str = day_data_str + str(slow_instant_utility[index]) + ","
                else:
                    day_data_str = day_data_str + ","

                if (len(hybrid_utility) > index):
                    day_data_str = day_data_str + str(hybrid_utility[index]) + ","
                else:
                    day_data_str = day_data_str + ","

                if (len(hybrid_ibl0_utility) > index):
                    day_data_str = day_data_str + str(hybrid_ibl0_utility[index]) + ","
                else:
                    day_data_str = day_data_str + ","

                if (len(hybrid_ibl1_utility) > index):
                    day_data_str = day_data_str + str(hybrid_ibl1_utility[index]) + ","
                else:
                    day_data_str = day_data_str + ","
                    
                day_data_str = day_data_str + ",,,"

                self.__writer.write(day_data_str)

            self.__writer.write("\n")
            index = index + 1

        self.__writer.write("\n")

        for day in days:
            trace_data = self.__result_db.get_trace_data(day)
                
            fast_aggregate_utility = trace_data.get_aggregate_fast_utility()
            slow_aggregate_utility = trace_data.get_aggregate_slow_utility()
            slow_instant_aggregate_utility = trace_data.get_aggregate_slow_instant_utility()
            hybrid_aggregate_utility = trace_data.get_aggregate_hybrid_utility()
            hybrid_ibl0_aggregate_utility = trace_data.get_aggregate_ibl0_utility()
            hybrid_ibl1_aggregate_utility = trace_data.get_aggregate_ibl1_utility()

            day_data_str = str(fast_aggregate_utility) + "," \
                    + str(slow_aggregate_utility) + "," \
                    + str(slow_instant_aggregate_utility) + "," \
                    + str(hybrid_aggregate_utility) + "," \
                    + str(hybrid_ibl0_aggregate_utility) + "," \
                    + str(hybrid_ibl1_aggregate_utility) + ",,,,"

            self.__writer.write(day_data_str)
            
        
        self.__writer.close()
        
