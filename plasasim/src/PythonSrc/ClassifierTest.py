import sys
from ClassifierTestDBWrapper import ClassifierTestDBWrapper
from sklearn.metrics import f1_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import confusion_matrix
from statistics import *

def get_day_from_trace(trace_path):
    elements = trace_path.split("/")
    day_info = elements[len(elements) - 1]
    skip_length = len("wc_day_")
    day = "Day"

    if (day_info[skip_length + 1] == "_"):
        day = day + day_info[skip_length:(skip_length + 1)]
    else:
        day = day + day_info[skip_length:(skip_length + 2)]

    return day

def main():
    print (sys.argv[1])
    print (sys.argv[2])
    file_name = sys.argv[2] + ".csv"

    db_wrapper = ClassifierTestDBWrapper(sys.argv[1], [])
    db_wrapper.read_db()
    ct_writer = open(file_name, "w+")
    ct_writer.write("Trace, Cons_violations, Cons_recall_0, Cons_recall_1, Cons_recall_2, \
            Cons_prec_0, Cons_prec_1, Cons_prec_2, \
            recall_0, recall_1, recall_2, \
            prec_0, prec_1, prec_2\n")
    
    
    traces = db_wrapper.get_all_traces()
    all_problems = db_wrapper.get_all_problems()
    rt_threshold = 1.0

    recall_0 = []
    recall_1 = []
    recall_2 = []

    precision_0 = []
    precision_1 = []
    precision_2 = []

    f1_trace = []
    f1_0 = []
    f1_1 = []
    f1_2 = []

    const_recall_0 = []
    const_recall_1 = []
    const_recall_2 = []

    const_precision_0 = []
    const_precision_1 = []
    const_precision_2 = []

    const_f1 = []
    const_f1_0 = []
    const_f1_1 = []
    const_f1_2 = []

    for trace in traces:
        day = get_day_from_trace(trace)
        #elements = trace.split('/')
        #day = elements[len(elements) - 1]
        ct_writer.write(day)
        ct_writer.write(", ")
        #trace_recall_0 = []
        #trace_recall_1 = []
        #trace_recall_1 = []

        #trace_precision_0 = []
        #trace_precision_1 = []
        #trace_precision_2 = []

        #trace_f1_0 = []
        #trace_f1_1 = []
        #trace_f1_2 = []

        #trace_const_recall_0 = []
        #trace_const_recall_1 = []
        #trace_const_recall_1 = []

        #trace_const_precision_0 = []
        #trace_const_precision_1 = []
        #trace_const_precision_2 = []

        #trace_const_f1_0 = []
        #trace_const_f1_1 = []
        #trace_const_f1_2 = []

        predicted_labels = []
        actual_labels = []
        constraint_violation_actual_labels = []
        constraint_violation_predicted_labels = []

        trace_problems = all_problems.get_trace_problems(trace)

        for trace_problem in trace_problems:
            if (trace_problem.get_response_time() >= rt_threshold): 
                constraint_violation_actual_labels.append(trace_problem.get_actual_label())
                constraint_violation_predicted_labels.append(trace_problem.get_label())

            actual_labels.append(trace_problem.get_actual_label())
            predicted_labels.append(trace_problem.get_label())


        label_0_present = 0 in constraint_violation_actual_labels
        label_1_present = 1 in constraint_violation_actual_labels
        label_2_present = 2 in constraint_violation_actual_labels
        label_0_predicted = 0 in constraint_violation_predicted_labels
        label_1_predicted = 1 in constraint_violation_predicted_labels
        label_2_predicted = 2 in constraint_violation_predicted_labels

        print("Trace = ", trace)
        print("################## Constraint Violation ############################")
        print("Actual Constraint Labels = ", constraint_violation_actual_labels)
        print("Predicted Constraint Lab = ", constraint_violation_predicted_labels)
        print("Confusion Matrix = ", confusion_matrix(constraint_violation_actual_labels, constraint_violation_predicted_labels))

        recall = recall_score(constraint_violation_actual_labels, constraint_violation_predicted_labels, average=None)
        print("recall_score = ", recall)
        ct_writer.write(str(len(constraint_violation_actual_labels)) + ", ")

        if (len(constraint_violation_actual_labels) == 0):
            ct_writer.write("inf, ")
            ct_writer.write("inf, ")
            ct_writer.write("inf, ")
            #print("Hi")
        else:
            if (label_0_present or label_0_predicted):
                const_recall_0.append(recall[0])
                ct_writer.write(str(recall[0]) + ", ")
                print("0")
            else:
                ct_writer.write("inf, ")
                print("1")

            if ((label_0_present or label_0_predicted) \
                and (label_1_present or label_1_predicted)):
                const_recall_1.append(recall[1])
                ct_writer.write(str(recall[1]) + ", ")
                if ((label_2_present or label_2_predicted)):
                    const_recall_2.append(recall[2])
                    ct_writer.write(str(recall[2]) + ", ")
                else:
                    ct_writer.write("inf, ")
                print("2")
            elif (not (label_0_present or label_0_predicted) \
                and (label_1_present or label_1_predicted)):
                const_recall_1.append(recall[0])
                ct_writer.write(str(recall[0]) + ", ")
                if ((label_2_present or label_2_predicted)):
                    const_recall_2.append(recall[1])
                    ct_writer.write(str(recall[1]) + ", ")
                else:
                    ct_writer.write("inf, ")
                print("3")
            elif ((label_0_present or label_0_predicted) \
                and (not (label_1_present or label_1_predicted))):
                ct_writer.write("inf, ")
                if ((label_2_present or label_2_predicted)):
                    const_recall_2.append(recall[1])
                    ct_writer.write(str(recall[1]) + ", ")
                else:
                    ct_writer.write("inf, ")
                print("4")
            else:
                ct_writer.write("inf, ")
                if ((label_2_present or label_2_predicted)):
                    const_recall_2.append(recall[0])
                    ct_writer.write(str(recall[0]) + ", ")
                else:
                    ct_writer.write("inf, ")

        prec_score = precision_score(constraint_violation_actual_labels, constraint_violation_predicted_labels, average=None)
        print("precision_score = ", prec_score)

        if (len(constraint_violation_actual_labels) == 0):
            ct_writer.write("inf, ")
            ct_writer.write("inf, ")
            ct_writer.write("inf, ")
        else:
            if (label_0_present or label_0_predicted):
                const_precision_0.append(prec_score[0])
                ct_writer.write(str(prec_score[0]) + ", ")
            else:
                ct_writer.write("inf, ")

            if ((label_0_present or label_0_predicted) \
                and (label_1_present or label_1_predicted)):
                const_precision_1.append(prec_score[1])
                ct_writer.write(str(prec_score[1]) + ", ")
                if ((label_2_present or label_2_predicted)):
                    const_precision_2.append(prec_score[2])
                    ct_writer.write(str(prec_score[2]) + ", ")
                else:
                    ct_writer.write("inf, ")
            elif (not (label_0_present or label_0_predicted) \
                and (label_1_present or label_1_predicted)):
                const_precision_1.append(prec_score[0])
                ct_writer.write(str(prec_score[0]) + ", ")
                if ((label_2_present or label_2_predicted)):
                    const_precision_2.append(prec_score[1])
                    ct_writer.write(str(prec_score[1]) + ", ")
                else:
                    ct_writer.write("inf, ")
            elif ((label_0_present or label_0_predicted) \
                and (not (label_1_present or label_1_predicted))):
                const_precision_2.append(prec_score[1])
                ct_writer.write("inf, ")
                if ((label_2_present or label_2_predicted)):
                    ct_writer.write(str(prec_score[1]) + ", ")
                else:
                    ct_writer.write("inf, ")
            else:
                ct_writer.write("inf, ")
                if ((label_2_present or label_2_predicted)):
                    const_precision_2.append(prec_score[0])
                    ct_writer.write(str(prec_score[0]) + ", ")
                else:
                    ct_writer.write("inf, ")
                               
        f1 = f1_score(constraint_violation_actual_labels, constraint_violation_predicted_labels, average=None)
        print("Constraint f1_score = ", f1)
        const_f1.append(f1)
        #const_f1_0.append(f1_score[0])
        #const_f1_1.append(f1_score[1])
        #const_f1_2.append(f1_score[2])

        label_0_present = 0 in actual_labels
        label_1_present = 1 in actual_labels
        label_0_predicted = 0 in predicted_labels
        label_1_predicted = 1 in predicted_labels
        print("################## Trace Overall ############################")
        print("Actual Labels = ", actual_labels)
        print("Predicted Lab = ", predicted_labels)
        print("Confusion Matrix = ", confusion_matrix(actual_labels, predicted_labels))
            
        recall = recall_score(actual_labels, predicted_labels, average=None)
        print("recall_score = ", recall)

        if (label_0_present or label_0_predicted):
            recall_0.append(recall[0])
            ct_writer.write(str(recall[0]) + ", ")
        else:
            ct_writer.write("inf, ")

        if ((label_0_present or label_0_predicted) \
            and (label_1_present or label_1_predicted)):
            recall_1.append(recall[1])
            ct_writer.write(str(recall[1]) + ", ")
            recall_2.append(recall[2])
            ct_writer.write(str(recall[2]) + ", ")
        elif (not (label_0_present or label_0_predicted) \
            and (label_1_present or label_1_predicted)):
            recall_1.append(recall[0])
            ct_writer.write(str(recall[0]) + ", ")
            recall_2.append(recall[1])
            ct_writer.write(str(recall[1]) + ", ")
        elif ((label_0_present or label_0_predicted) \
            and (not (label_1_present or label_1_predicted))):
            recall_2.append(recall[1])
            ct_writer.write("inf, ")
            ct_writer.write(str(recall[1]) + ", ")
        else:
            ct_writer.write("inf, ")
            ct_writer.write(str(recall[0]) + ", ")

        prec_score = precision_score(actual_labels, predicted_labels, average=None)
        print("precision_score = ", prec_score)

        if (label_0_present or label_0_predicted):
            precision_0.append(prec_score[0])
            ct_writer.write(str(prec_score[0]) + ", ")
        else:
            ct_writer.write("inf, ")

        if ((label_0_present or label_0_predicted) \
            and (label_1_present or label_1_predicted)):
            precision_1.append(prec_score[1])
            ct_writer.write(str(prec_score[1]) + ", ")
            precision_2.append(prec_score[2])
            ct_writer.write(str(prec_score[2]) + ", ")
        elif (not (label_0_present or label_0_predicted) \
            and (label_1_present or label_1_predicted)):
            precision_1.append(prec_score[0])
            ct_writer.write(str(prec_score[0]) + ", ")
            precision_2.append(prec_score[1])
            ct_writer.write(str(prec_score[1]) + ", ")
        elif ((label_0_present or label_0_predicted) \
            and (not (label_1_present or label_1_predicted))):
            precision_2.append(prec_score[1])
            ct_writer.write("inf, ")
            ct_writer.write(str(prec_score[1]) + ", ")
        else:
            ct_writer.write("inf, ")
            ct_writer.write(str(prec_score[0]) + ", ")
                               
        f1 = f1_score(actual_labels, predicted_labels, average=None)
        print("Trace f1_score = ", f1)
        f1_trace.append(f1)
        ct_writer.write("\n")
        #f1_0.append(f1[0])
        #f1_1.append(f1[1])
        #f2_2.append(f1[2])

    print("All mean const recall_0 = ", mean(const_recall_0))
    print("All mean const recall_1 = ", mean(const_recall_1))
    print("All mean const recall_2 = ", mean(const_recall_2))

    print("All mean const precision_0 = ", mean(const_precision_0))
    print("All mean const precision_1 = ", mean(const_precision_1))
    print("All mean const precision_2 = ", mean(const_precision_2))

    #print("All mean const f1 = ", mean(const_f1))
    #print("All mean const f1_1 = ", mean(const_f1_1))
    #print("All mean const f1_2 = ", mean(const_f1_2))

    print("All mean recall_0 = ", mean(recall_0))
    print("All mean recall_1 = ", mean(recall_1))
    print("All mean recall_2 = ", mean(recall_2))

    print("All mean precision_0 = ", mean(precision_0))
    print("All mean precision_1 = ", mean(precision_1))
    print("All mean precision_2 = ", mean(precision_2))

    #print("All mean f1 = ", mean(f1_trace))
    #print("All mean f1_1 = ", mean(f1_1))
    #print("All mean f1_2 = ", mean(f1_2))
    ct_writer.close()

if __name__ == "__main__":
    sys.exit(main())
