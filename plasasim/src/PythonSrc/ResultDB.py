import sys
from collections import defaultdict

class ResultDB:
    def __init__(self):
        self.__trace_db = defaultdict()

    def set_trace_data(self, day, trace_data):
        self.__trace_db[day] = trace_data

    
    def get_trace_data(self, day):
        return self.__trace_db[day]

    def get_days(self):
        days = list()

        for key in self.__trace_db:
            days.append(key)

        return days
