
class TraceData:
    def __init__(self, day):
        self.__day = day
        self.__fast_utility = ""
        self.__slow_utility = ""
        self.__hybrid_utility = ""
        self.__hybrid_ibl_0_utility = ""
        self.__hybrid_ibl_1_utility = ""
        self.__slow_instant_utility = ""

    def set_fast_utility(self, utility):
        self.__fast_utility = utility

    def get_fast_utility(self):
        return self.__fast_utility

    def set_slow_utility(self, utility):
        self.__slow_utility = utility

    def get_slow_utility(self):
        return self.__slow_utility

    def set_hybrid_utility(self, utility):
        self.__hybrid_utility = utility

    def get_hybrid_utility(self):
        return self.__hybrid_utility

    def set_ibl0_utility(self, utility):
        self.__hybrid_ibl_0_utility = utility

    def get_ibl0_utility(self):
        return self.__hybrid_ibl_0_utility

    def set_ibl1_utility(self, utility):
        self.__hybrid_ibl_1_utility = utility

    def get_ibl1_utility(self):
        return self.__hybrid_ibl_1_utility

    def set_slow_instant_utility(self, utility):
        self.__slow_instant_utility = utility

    def get_slow_instant_utility(self):
        return self.__slow_instant_utility

    def get_aggregate_fast_utility(self):
        return sum(self.get_fast_utility())

    def get_aggregate_slow_utility(self):
        return sum(self.get_slow_utility())

    def get_aggregate_slow_instant_utility(self):
        return sum(self.get_slow_instant_utility())

    def get_aggregate_hybrid_utility(self):
        return sum(self.get_hybrid_utility())

    def get_aggregate_ibl0_utility(self):
        return sum(self.get_ibl0_utility())

    def get_aggregate_ibl1_utility(self):
        return sum(self.get_ibl1_utility())  
