import sys
from timeit import default_timer as timer
import time
#from ProblemDatabase import ProblemFeatures
#from ProblemDatabase import ProblemDatabase
#from ProblemDatabase import ProblemInstance
#from ProblemDatabase import DBWrapper
from DBWrapper import DBWrapper
from sklearn.ensemble import ExtraTreesClassifier

print("Sourcing file")

class ETClassifier:
    __slots__ = ['__data_file', '__skip_trace', '__db', '__clf', '__compare_past_workload_for_similarity']
    
    def __init__(self, data_file, skip_trace, estimators_count = 85, \
                    compare_past_workload_for_similarity = 0):
        print("Ignoring Trace while training = ", skip_trace)
        print("Estimators Count = ", estimators_count)
        print("Compare past workload = ", compare_past_workload_for_similarity)
        self.__data_file = data_file
        self.__skip_trace = skip_trace
        self.__db = DBWrapper(data_file, [skip_trace], compare_past_workload_for_similarity)
        self.__clf = ExtraTreesClassifier(n_estimators=estimators_count)
        self.__compare_past_workload_for_similarity = compare_past_workload_for_similarity
        
    def train(self):
        print("Inside train")
        self.__db.read_db()
        all_training_problems = self.__db.get_all_training_problems()
        self.__clf.fit(all_training_problems.get_features_list(), all_training_problems.get_label_list())
        print("Exiting Train")
        return 1

    def predict(self, features):
        print("Inside predict", features)
        t1 = timer()
        prediction = self.__clf.predict(features)
        #print("Prediction time in seconds = ", timer() - t1)

        return prediction

    #def fun(self):
        #print("Inside fun")
        #return 1

    def get_request_arrival_rate(self, rar):
        #print("rar = ", rar)
        fixed_rar = float(rar)
        #print("fixed_rar = ", fixed_rar)
    
        if (fixed_rar == float('inf')):
            fixed_rar = sys.maxsize #sys.long_info.max #2147483647
            #sys.float_info.max
            print("Changed fixed_rar = ", fixed_rar)

        return fixed_rar

    def classifier_predict(self, dimmer, serverA_count, serverB_count, \
                serverC_count, serverA_status, serverB_status, \
                serverC_status, serverA_load,  serverB_load, \
                serverC_load, avg_response_time, r1, r2, r3, \
		        r4, r5, r6, r7, r8, r9, \
                r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20):
        #print("Inside classifier_predict", dimmer)
        features = list()
        
        features.append(float(dimmer))
        features.append(float(serverA_count)) 
        features.append(float(serverB_count)) 
        features.append(float(serverC_count)) 
        features.append(float(serverA_status)) 
        features.append(float(serverB_status)) 
        features.append(float(serverC_status)) 
        features.append(float(serverA_load)) 
        features.append(float(serverB_load)) 
        features.append(float(serverC_load))
        features.append(float(avg_response_time))

        if (self.__compare_past_workload_for_similarity == 1):
            features.append(self.get_request_arrival_rate(r1)) 
            features.append(self.get_request_arrival_rate(r2)) 
            features.append(self.get_request_arrival_rate(r3)) 
            features.append(self.get_request_arrival_rate(r4)) 
            features.append(self.get_request_arrival_rate(r5)) 
            features.append(self.get_request_arrival_rate(r6)) 
            features.append(self.get_request_arrival_rate(r7)) 
            features.append(self.get_request_arrival_rate(r8)) 
            features.append(self.get_request_arrival_rate(r9)) 
            features.append(self.get_request_arrival_rate(r10)) 
            features.append(self.get_request_arrival_rate(r11)) 
            features.append(self.get_request_arrival_rate(r12)) 
            features.append(self.get_request_arrival_rate(r13)) 
            features.append(self.get_request_arrival_rate(r14))

        features.append(self.get_request_arrival_rate(r15))
        features.append(self.get_request_arrival_rate(r16)) 
        features.append(self.get_request_arrival_rate(r17)) 
        features.append(self.get_request_arrival_rate(r18)) 
        features.append(self.get_request_arrival_rate(r19)) 
        features.append(self.get_request_arrival_rate(r20)) 
      
        prediction = self.predict([features])

        #print ("prediction = ", prediction[0])

        return int(prediction[0])
   
def main():
    classifier = None
    compare_past_workload_for_similarity = 0
    estimator_count = 87

    if (len(sys.argv) > 3):
        estimator_count = int(sys.argv[3])
    
    if (len(sys.argv) > 4):
        compare_past_workload_for_similarity = int(sys.argv[4])

    classifier = ETClassifier(sys.argv[1], sys.argv[2], estimator_count, \
                    compare_past_workload_for_similarity)

    classifier.train()
    #t1 = current_time()
    #start = time.time()
    result = classifier.classifier_predict(1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 4.0, 0.008389, \
                    12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, \
                    119.205145, 234.066441, str('inf'), str('inf'), str('inf'), str('inf'))
    #str(1), str(2), str(3), str(4), str(5), str(6), str(7),\
                                            #str(8), str(9), str(10), str(11), str(12), \
                                            #str(13), str(14), str(15), str(15), str(16), \
                                            #str(17), str(18), str(19), str(20), str(21), \
                                            #str(22), str(23), str(24), str(25), str(26), \
                                            #str(27), str(28), str(29), str(30))
    #result = classifier.predict([[1,2,3,4,5,6,7,8,9, 10, 11, float(12), float(13), \
    #                    float(14), float(15), float(16), float(17), float(18), float(19), \
    #                    float(20), float(21), float(22), float(23), \
    #                    float(24), float(25), float(132.222),  float(141.602),  \
    #                    float(147.459),  float(153.029),  float(158.291),  float(163.23)]])
    #end = time.time()
    #print(end - start)
    #print(current_time() - t1)
    print("Prediction =", result)
    return result
            
if __name__ == "__main__":
    sys.exit(main())
