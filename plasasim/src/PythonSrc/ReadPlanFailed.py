import sys
from collections import defaultdict

def get_info(path):
    elements = path.split('/')

    return elements[0], elements[1]
    
def main():
    file = open("Failed", "r")
    print("File = ", file) 
    data_base = defaultdict(list)
    failed_count_writer = open("Failed_count.csv", "w+")
    failed_count_writer.write("Trace, Fast, Slow, Hybrid, Hybrid_IBL0, Hybrid_IBL1\n")

    for line in file: 
        #print(line)
        day, mode = get_info(line)

        if day not in data_base.keys():
            data_base[day] = [0, -1, -1, -1, -1] # -1 because first one failed by default


        if (mode == "Fast"):
            (data_base[day])[0] = (data_base[day])[0] + 1
        if (mode == "Slow"):
            (data_base[day])[1] = (data_base[day])[1] + 1
        if (mode == "Hybrid"):
            (data_base[day])[2] = (data_base[day])[2] + 1
        if (mode == "Hybrid_IBL0"):
            (data_base[day])[3] = (data_base[day])[3] + 1
        if (mode == "Hybrid_IBL1"):
            (data_base[day])[4] = (data_base[day])[4] + 1

    file.close()

    # Now dump the data
    for day in data_base.keys():
        line_str = day + ", " +  str((data_base[day])[0]) \
                + ", " +  str((data_base[day])[1]) \
                + ", " +  str((data_base[day])[2]) \
                + ", " +  str((data_base[day])[3]) \
                + ", " +  str((data_base[day])[4]) + "\n"

        failed_count_writer.write(line_str)

    
    failed_count_writer.close()


if __name__ == "__main__":
    sys.exit(main())
