from sklearn.neighbors import KNeighborsClassifier
import sys
from collections import defaultdict

# TODO this class needs to be merged in class ProblemDatabase
class ProblemFeatures:
    #__slots__ = ['__dimmer', '__serverA_count', '__serverB_count', '__serverC_count', \
    #             '__serverA_status', '__serverB_status', '__serverC_status', \
    #             '__serverA_load', '__serverB_load', '__serverC_load', \
    #            '__r1', '__r2', '__r3', '__r4', '__r5', '__r6', '__r7', \
    #            '__r8', '__r9', '__r10', '__r11', '__r12' \
    #            '__r13', '__r14', '__r15', '__r16', '__r17', \
    #            '__r18', '__r19', '__r20', '__r21']

    def __init__(self, dimmer, serverA_count, serverB_count, serverC_count, serverA_status, \
            serverB_status, serverC_status, serverA_load,  serverB_load,  \
            serverC_load, avg_response_time, workload):
        #print("Inside ProblemFestures::init")
        self.__dimmer = int(dimmer)
        self.__serverA_count = int(serverA_count)
        self.__serverB_count = int(serverB_count)
        self.__serverC_count = int(serverC_count)
        self.__serverA_status = int(serverA_status)
        self.__serverB_status = int(serverB_status)
        self.__serverC_status = int(serverC_status)
        self.__serverA_load = int(serverA_load)
        self.__serverB_load = int(serverB_load)
        self.__serverC_load = int(serverC_load)
        self.__avg_response_time = float(avg_response_time)
        
        #workload = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        
        self.__r1 = float(workload[0])
        self.__r2 = float(workload[1])
        self.__r3 = float(workload[2])
        self.__r4 = float(workload[3])
        self.__r5 = float(workload[4])
        self.__r6 = float(workload[5])
        self.__r7 = float(workload[6])
        self.__r8 = float(workload[7])
        self.__r9 = float(workload[8])
        self.__r10 = float(workload[9])
        self.__r11 = float(workload[10])
        self.__r12 = float(workload[11])
        self.__r13 = float(workload[12])
        self.__r14 = float(workload[13])
        self.__r15 = float(workload[14])
        self.__r16 = float(workload[15])
        self.__r17 = float(workload[16])
        self.__r18 = float(workload[17])
        self.__r19 = float(workload[18])
        self.__r20 = float(workload[19])
        #self.__r21 = workload[20]
        #self.__workload = workload

    def find_similarity(self, problem):
        self.__serverA_count - problem.__serverA_count \
               + TimeSeriesSimilarity(self.__workload, problem.__workload)

    def print_features(self):
        print(self.__dimmer)
        print(self.__serverA_count)
        print(self.__serverB_count)
        print(self.__serverC_count)
        print(self.__serverA_status)
        print(self.__serverB_status)
        print(self.__serverC_status)
        print(self.__serverA_load)
        print(self.__serverB_load)
        print(self.__serverC_load)
        print(self.__avg_response_time)
        print(self.__r1)
        print(self.__r2)
        print(self.__r3)
        print(self.__r4)
        print(self.__r5)
        print(self.__r6)
        print(self.__r7)
        print(self.__r8)
        print(self.__r9)
        print(self.__r10)
        print(self.__r11)
        print(self.__r12)
        print(self.__r13)
        print(self.__r14)
        print(self.__r15)
        print(self.__r16)
        print(self.__r17)
        print(self.__r18)
        print(self.__r19)
        print(self.__r20)
        print(self.__junk)
        #print(self.__r21)

    def get_dimmer(self):
        return self.__dimmer

    def get_serverA_count(self):
        return self.__serverA_count

    def get_serverB_count(self):
        return self.__serverB_count

    def get_serverC_count(self):
        return self.__serverC_count

    def get_serverA_status(self):
        return self.__serverA_status

    def get_serverB_status(self):
        return self.__serverB_status

    def get_serverC_status(self):
        return self.__serverC_status

    def get_serverA_load(self):
        return self.__serverA_load

    def get_serverB_load(self):
        return self.__serverB_load

    def get_serverC_load(self):
        return self.__serverC_load

    def get_avg_response_time(self):
        return self.__avg_response_time

    def get_r1(self):
        return self.__r1

    def get_r2(self):
        return self.__r2

    def get_r3(self):
        return self.__r3

    def get_r4(self):
        return self.__r4

    def get_r5(self):
        return self.__r5

    def get_r6(self):
        return self.__r6

    def get_r7(self):
        return self.__r7

    def get_r8(self):
        return self.__r8

    def get_r9(self):
        return self.__r9

    def get_r10(self):
        return self.__r10

    def get_r11(self):
        return self.__r11

    def get_r12(self):
        return self.__r12

    def get_r13(self):
        return self.__r13

    def get_r14(self):
        return self.__r14

    def get_r15(self):
        return self.__r15

    def get_r16(self):
        return self.__r16

    def get_r17(self):
        return self.__r17

    def get_r18(self):
        return self.__r18

    def get_r19(self):
        return self.__r19

    def get_r20(self):
        return self.__r20

    #def get_r21(self):
        #return self.__r21


        #for rate in self.__workload:
        #    print(rate)

class ProblemInstance:
    __slots__ = ['__trace_name', '__profiling_dir', '__fast_dir', '__slow_dir', \
                    '__features', '__label', \
                    '__compare_past_workload_for_similarity', '__actual_label']

    def __init__(self, trace_name, profiling_dir, fast_dir, slow_dir, \
                        features, label, compare_past_workload_for_similarity = 1, \
                        actual_label = -1):
        self.__trace_name = trace_name
        self.__profiling_dir = profiling_dir
        self.__fast_dir = fast_dir
        self.__slow_dir = slow_dir

        self.__features = list()

        self.__features.append(features.get_dimmer())
        self.__features.append(features.get_serverA_count())
        self.__features.append(features.get_serverB_count())
        self.__features.append(features.get_serverC_count())
        self.__features.append(features.get_serverA_status())
        self.__features.append(features.get_serverB_status())
        self.__features.append(features.get_serverC_status())
        self.__features.append(features.get_serverA_load())
        self.__features.append(features.get_serverB_load())
        self.__features.append(features.get_serverC_load())
        self.__features.append(features.get_avg_response_time())
        self.__features.append(float(features.get_r1()))
        self.__features.append(float(features.get_r2()))
        self.__features.append(float(features.get_r3()))
        self.__features.append(float(features.get_r4()))
        self.__features.append(float(features.get_r5()))
        self.__features.append(float(features.get_r6()))
        self.__features.append(float(features.get_r7()))
        self.__features.append(float(features.get_r8()))
        self.__features.append(float(features.get_r9()))
        self.__features.append(float(features.get_r10()))
        self.__features.append(float(features.get_r11()))
        self.__features.append(float(features.get_r12()))
        self.__features.append(float(features.get_r13()))
        self.__features.append(float(features.get_r14()))
        self.__features.append(float(features.get_r15()))
        self.__features.append(float(features.get_r16()))
        self.__features.append(float(features.get_r17()))
        self.__features.append(float(features.get_r18()))
        self.__features.append(float(features.get_r19()))
        self.__features.append(float(features.get_r20()))
        #self.__features.append(float(features.get_r21()))

        self.__compare_past_workload_for_similarity = compare_past_workload_for_similarity
        
        #print(len(self.__features))
        
        self.__label = label
        self.__actual_label = actual_label

    def get_trace(self):
        return self.__trace_name

    def get_response_time(self):
        features = self.get_features()
        return features[10]

    def print_instance(self):
        print(self.__trace_name)
        print(self.__profiling_dir)
        print(self.__fast_dir)
        print(self.__slow_dir)
        print(self.__features)
        print(self.__label)

    def get_features(self):
        #print("Original features = ", self.__features)
        features = list()
        if (self.__compare_past_workload_for_similarity == 1):
            features = self.__features
        else:
            i = 0
            #print("features count = ", len(self.__features))
            while (i < 11):
                #print("index = ", i)
                features.append(self.__features[i])
                i = i + 1

            i = len(self.__features) - 6
            
            while (i < len(self.__features)):
                features.append(self.__features[i])
                i = i + 1
            
        #print("features = ", features)
        return features
        
    def get_label(self):
        return self.__label

    def get_actual_label(self):
        return self.__actual_label

class ProblemDatabase:
    __slots__ = ['__features_list', '__labels_list', '__problems']
    
    def __init__(self):
        #print("Inside ProblemDatabase::init")
        self.__features_list = list()
        self.__labels_list = list()
        self.__problems = list()

    def get_features_list(self):
        return self.__features_list

    def get_label_list(self):
        return self.__labels_list

    def get_problems(self):
        return self.__problems

    def get_trace_problems(self, trace):
        trace_problems = list()

        for problem in self.__problems:
            if (problem.get_trace() == trace):
                trace_problems.append(problem)

        return trace_problems

    def add_data(self, problem):
        #features_temp = list()
        #features_temp.append(problem.get_features())

        self.__features_list.append(problem.get_features())
        #print(self.__features_list)
        self.__labels_list.append(problem.get_label())
        self.__problems.append(problem)
    
    def get_db_size(self):
        return len(self.__problems)

if __name__ == '__main__':
    pb_db = ProblemDatabase()

    workload = list()
    workload.append(100)
    workload.append(200)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)
    workload.append(300)

    features = ProblemFeatures(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, workload)
    #features.print_features()
    pb_inst = ProblemInstance("a", "b", "c", "d", features, 1)
    #pb_inst.print_instance()
    pb_db.add_data(pb_inst)
    pb_db.add_data(pb_inst)
    
    neigh = KNeighborsClassifier(n_neighbors=3)
    X = pb_db.get_features_list()
    y = pb_db.get_label_list()

    print("X = ", X)
    print("y = ", y)
    neigh.fit(X, y)
    
