from ResultDB import ResultDB
from TraceData import TraceData
import glob
#import os.path
from pathlib import Path

class ReadResult:
    def __init__(self, parent_dir, fast, slow, hybrid, hybrid_ibl_0, hybrid_ibl_1, slow_instant):
        self.__parent_dir = parent_dir
        self.__result_db = ResultDB()
        self.__read_fast = fast
        self.__read_slow_instant = slow_instant
        self.__read_slow = slow
        self.__read_hybrid = hybrid
        self.__read_hybrid_ibl_0 = hybrid_ibl_0
        self.__read_hybrid_ibl_1 = hybrid_ibl_1

        self.__populate_db()

    def get_result_db(self):
        return self.__result_db

    def __get_trace_dirs(self):
        path = self.__parent_dir + "/Day*"
        #print("path = ", path)
        return glob.glob(path)

    def __read_utility(self, utility_file):
        utilities = list()
        
        if (Path(utility_file).is_file()): 
            utility_reader = open(utility_file, 'r')

            rows = 0
            zero_found = False
            prev_utility = 0

            for line in utility_reader:
                rows = rows + 1
                line = line.strip('\n')
                utility = float(line)

                #print("utility = ", utility)

                if (zero_found):
                    utilities.append(utility)

                if (not zero_found and utility == 0.0):
                    zero_found = True
                    utilities.append(prev_utility)

                prev_utility = utility

            #assert(len(utilities) == 90)
        else:
            print(utility_file, " NOT FOUND.") 

        return utilities
    
    def __populate_db(self):
        dirs = self.__get_trace_dirs()
        #print("dirs = ", dirs)
        #assert(len(dirs) == 87)

        for directory in dirs:
            trace_data = TraceData(directory)
            
            if (self.__read_fast):
                fast_utility = self.__read_utility(directory + "/Fast/utility")
                trace_data.set_fast_utility(fast_utility)

            if (self.__read_slow):
                slow_utility = self.__read_utility(directory + "/Slow/utility")
                trace_data.set_slow_utility(slow_utility)

            if (self.__read_slow_instant):
                slow_instant_utility = self.__read_utility(directory + "/SlowInstant/utility")
                trace_data.set_slow_instant_utility(slow_instant_utility)

            if (self.__read_hybrid):
                hybrid_utility = self.__read_utility(directory + "/Hybrid/utility")
                trace_data.set_hybrid_utility(hybrid_utility)

            if (self.__read_hybrid_ibl_0):
                ibl0_utility = self.__read_utility(directory + "/Hybrid_IBL0/utility")
                trace_data.set_ibl0_utility(ibl0_utility)

            if (self.__read_hybrid_ibl_1):
                ibl1_utility = self.__read_utility(directory + "/Hybrid_IBL1/utility")
                trace_data.set_ibl1_utility(ibl1_utility)

            self.__result_db.set_trace_data(directory, trace_data)
