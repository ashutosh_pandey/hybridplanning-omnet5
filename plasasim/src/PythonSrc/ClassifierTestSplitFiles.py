import sys

def get_day_from_trace(trace_path):
    elements = trace_path.split("/")
    day_info = elements[len(elements) - 1]
    skip_length = len("wc_day_")
    day = "Day"

    if (day_info[skip_length + 1] == "_"):
        day = day + day_info[skip_length:(skip_length + 1)]
    else:
        day = day + day_info[skip_length:(skip_length + 2)]

    return day

def main():
    print (sys.argv[1])
    print (sys.argv[2])

    wins_writer = open(sys.argv[2] + "_wins.csv", "w+")
    same_writer = open(sys.argv[2] + "_same.csv", "w+")
    looses_writer = open(sys.argv[2] + "_looses.csv", "w+")

    ibl0_wins = ['Day12', 'Day18', 'Day19', 'Day20', 'Day24', 'Day25', 'Day26', 'Day29', 'Day31', 'Day36', 'Day37', 'Day40', 'Day50', 'Day51', 'Day56', 'Day57', 'Day58', 'Day59', 'Day6', 'Day60', 'Day61', 'Day62', 'Day63', 'Day64', 'Day65', 'Day66', 'Day67', 'Day68', 'Day7', 'Day70', 'Day72', 'Day73', 'Day74', 'Day76', 'Day78', 'Day85', 'Day87', 'Day9']
    ibl0_same = ['Day15', 'Day16', 'Day21', 'Day27', 'Day28', 'Day30', 'Day32', 'Day33', 'Day34', 'Day35', 'Day38', 'Day41', 'Day42', 'Day43', 'Day47', 'Day52', 'Day53', 'Day54', 'Day55', 'Day71', 'Day77', 'Day79', 'Day8', 'Day80', 'Day81', 'Day83', 'Day84', 'Day88', 'Day89', 'Day90', 'Day91', 'Day92']
    ibl0_looses = ['Day10', 'Day11', 'Day13', 'Day14', 'Day17', 'Day22', 'Day23', 'Day39', 'Day44', 'Day45', 'Day46', 'Day48', 'Day49', 'Day69', 'Day75', 'Day82', 'Day86']

    ibl1_wins = ['Day12', 'Day18', 'Day19', 'Day25', 'Day26', 'Day31', 'Day33', 'Day36', 'Day37', 'Day38', 'Day39', 'Day40', 'Day41', 'Day45', 'Day48', 'Day50', 'Day51', 'Day56', 'Day57', 'Day59', 'Day6', 'Day61', 'Day63', 'Day64', 'Day65', 'Day66', 'Day67', 'Day68', 'Day69', 'Day7', 'Day70', 'Day73', 'Day76', 'Day78', 'Day85', 'Day87', 'Day9']
    ibl1_same =  ['Day15', 'Day16', 'Day21', 'Day27', 'Day28', 'Day30', 'Day32', 'Day35', 'Day42', 'Day43', 'Day47', 'Day55', 'Day71', 'Day77', 'Day79', 'Day8', 'Day80', 'Day81', 'Day83', 'Day84', 'Day86', 'Day89', 'Day90', 'Day91', 'Day92']
    ibl1_looses = ['Day10', 'Day11', 'Day13', 'Day14', 'Day17', 'Day20', 'Day22', 'Day23', 'Day24', 'Day29', 'Day34', 'Day44', 'Day46', 'Day49', 'Day52', 'Day53', 'Day54', 'Day58', 'Day60', 'Day62', 'Day72', 'Day74', 'Day75', 'Day82', 'Day88']

    db_csvfile = open(sys.argv[1], 'r')
    rows = 0

    for line in db_csvfile:
            rows = rows + 1
            if (rows == 1):
                # TODO Make sure the first line is a header
                wins_writer.write(line)
                same_writer.write(line)
                looses_writer.write(line)
                continue
            
            # Remove newline at the end
            line = line.strip('\n')
            #print(line)

            tokens = line.split(',')
            #print(tokens)

            #tokens = re.split(',', line)
            trace = tokens[0]
            day = get_day_from_trace(trace)

            if (sys.argv[2] == "ibl0"):
                if (day in ibl0_wins):
                    wins_writer.write(line + '\n')
                elif (day in ibl0_same):
                    same_writer.write(line + '\n')
                else:
                    looses_writer.write(line + '\n')
            else:
                if (day in ibl1_wins):
                    wins_writer.write(line + '\n')
                elif (day in ibl1_same):
                    same_writer.write(line + '\n')
                else:
                    looses_writer.write(line + '\n')

    wins_writer.close()
    same_writer.close()
    looses_writer.close()
    db_csvfile.close()

if __name__ == "__main__":
    sys.exit(main())
