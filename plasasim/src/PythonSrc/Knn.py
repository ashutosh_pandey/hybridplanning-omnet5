import os, sys
#from sets import Set
from timeit import default_timer as timer
import time
from ProblemDatabase import *
from sklearn.neighbors import KNeighborsClassifier
from TimeSeriesSimilarity import TimeSeriesSimilarity

comp_length = 0

def current_time():
    return timer()

#Write similarity metric function
def calculate_similarity(features1, features2):
    #print("comp_length = ", comp_length)

    time_series1 = features1[len(features1) - comp_length:len(features1)]
    time_series2 = features2[len(features2) - comp_length:len(features2)]

    #print("time_series1 = ", time_series1)
    #print("time_series2 = ", time_series2)

    load_similarity = TimeSeriesSimilarity(time_series1, time_series2)
    #print ("load_similarity = ", load_similarity)
    return load_similarity

class KnnClassifier:
    __slots__ = ['__db_file', '__problem_db', '__neighbour_count', '__algorithm', \
            '__knn', '__pb_db']

    def __init__(self, db_file, comparision_length, neighbour_count = 3, algorithm = 'ball_tree'):
        self.__db_file = db_file
        self.__problem_db = ProblemDatabase()
        
        global comp_length 
        comp_length = comparision_length

        self.__neighbour_count = neighbour_count
        self.__algorithm = algorithm

        self.__knn = KNeighborsClassifier(n_neighbors = self.__neighbour_count,\
                algorithm = self.__algorithm, metric = calculate_similarity)
        self.__pb_db = ProblemDatabase()

        print(self.__knn);
        #assert (not (starId is None)) and (not (trace is None))

    # reads csv file + trains
    def populate_db(self):
        # Read Db file
        #csv.field_size_limit(sys.maxsize)

        #num_rows = sum(1 for line in open(db_filename))
        print ("Reading problem database file from", self.__db_file)
 
       # Open the same file to compare with itself
        db_csvfile = open(self.__db_file, 'r')

        #print (db_csvfile)

        rows = 0
        count = 0;

        for line in db_csvfile:
            rows = rows + 1
            if (rows == 1):
                # TODO Make sure the first line is a header
                continue
            
            # Remove newline at the end
            line = line.strip('\n')
            #print(line)

            tokens = line.split(',')
            #print(tokens)

            #tokens = re.split(',', line)
            trace = tokens[0]
            #print("trace = ", trace)
            profiling_dir = tokens[1]
            #print("profiling_dir = ", profiling_dir)
            fast_dir = tokens[2]
            #print("fast_dir = ", fast_dir)
            slow_dir = tokens[3]
            #print("slow_dir = ", slow_dir)
            features = tokens[4:len(tokens) - 1]
            #print("tokens = ", tokens)
            #print("features = ", features)
            #print("workload = ", features[11:len(features)])
            prob_features = ProblemFeatures(features[0], features[1], \
                    features[2], features[3], features[4], features[5],\
                    features[6], features[7], features[8], features[9], \
                    features[10], features[11:len(features)])
            #prob_features.print_features()
            label = tokens[len(tokens) - 1]
            #label.strip()
            #print("label = ", label)

            problemInstance = ProblemInstance(trace, profiling_dir, \
                    fast_dir, slow_dir, prob_features, label)
            #problemInstance.print_instance()
            self.__pb_db.add_data(problemInstance)
            #break

        db_csvfile.close()

        #print(self.__pb_db.get_features_list())
        #print(self.__pb_db.get_label_list())

        #self.fit(self.__pb_db.get_features_list(), self.__pb_db.get_label_list())

        return 1

    def predict(self,  features):
        print("Inside predict", features)
        t1 = current_time()
        
        prediction = self.__knn.predict(features)
        
        print("Prediction time in seconds = ", current_time() - t1)

        return prediction

    def test(self, a, b):
        print ("a = ", a)
        print ("b = ", b)

    def knn_predict(self, dimmer, serverA_count, serverB_count, \
                serverC_count, serverA_status, serverB_status, \
                serverC_status, serverA_load,  serverB_load, \
                serverC_load, avg_response_time, r1, r2, r3, \
		r4, r5, r6, r7, r8, r9, \
                r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20):
        p#rint("Inside knn_predict", dimmer)
        features = list()

        features.append(float(dimmer))
        features.append(float(serverA_count)) 
        features.append(float(serverB_count)) 
        features.append(float(serverC_count)) 
        features.append(float(serverA_status)) 
        features.append(float(serverB_status)) 
        features.append(float(serverC_status)) 
        features.append(float(serverA_load)) 
        features.append(float(serverB_load)) 
        features.append(float(serverC_load))
        features.append(float(avg_response_time)) 
        features.append(float(r1)) 
        features.append(float(r2)) 
        features.append(float(r3)) 
        features.append(float(r4)) 
        features.append(float(r5)) 
        features.append(float(r6)) 
        features.append(float(r7)) 
        features.append(float(r8)) 
        features.append(float(r9)) 
        features.append(float(r10)) 
        features.append(float(r11)) 
        features.append(float(r12)) 
        features.append(float(r13)) 
        features.append(float(r14)) 
        features.append(float(r15)) 
        features.append(float(r16)) 
        features.append(float(r17)) 
        features.append(float(r18)) 
        features.append(float(r19)) 
        features.append(float(r20)) 
        #features.append(float(r21)) 
      
        prediction = self.predict([features])

        #print ("prediction = ", prediction[0])

        return int(prediction[0])

    def fit(self):
        self.__knn.fit(self.__pb_db.get_features_list(), self.__pb_db.get_label_list())
        return 1

    def train(self):
        self.populate_db()
        self.fit()

        return 1

    def cross_validate(self):
        #80:20
        #train
        #predict
        return 1

def main():
    knn = None
    knn = KnnClassifier("features2.csv", 20)
    #knn.populate_db()
    #knn.fit()
    knn.train()
    #t1 = current_time()
    #start = time.time()
    result = knn.predict([[1,2,3,4,5,6,7,8,9, 10, 11, float(12), float(13), \
                        float(14), float(15), float(16), float(17), float(18), float(19), \
                        float(20), float(21), float(22), float(23), \
                        float(24), float(25), float(132.222),  float(141.602),  \
                        float(147.459),  float(153.029),  float(158.291),  float(163.23)]])
    #end = time.time()
    #print(end - start)
    #print(current_time() - t1)

    return result
            
if __name__ == "__main__":
    sys.exit(main())
