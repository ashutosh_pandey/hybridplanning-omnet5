/*
 * EstimatorReader.h
 *
 *  Created on: Jul 18, 2018
 *      Author: ashutosp
 */

#ifndef MACHINELEARNING_ESTIMATORREADER_H_
#define MACHINELEARNING_ESTIMATORREADER_H_

using namespace std;

#include <string>
#include <map>
#include <fstream>


class EstimatorReader {
private:
    string m_input_file;
    std::map<string, unsigned> m_estimator_db;

    void ReadFile();

public:
    EstimatorReader(const string& input_file);
    virtual ~EstimatorReader();

    unsigned GetEstimatorCount(string trace_name);
};

#endif /* MACHINELEARNING_ESTIMATORREADER_H_ */
