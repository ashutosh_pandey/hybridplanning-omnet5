//===============================================================================
// Name        : ClassifyProblemInstance.cpp
// Author      : Ashutosh Pandey
// Version     :
// Copyright   : Your copyright notice
// Description : Bridge between C++ and Python
// ===============================================================================

#include <ClassifyProblemInstance.h>
#include <fstream>
#include <EstimatorReader.h>
#include <DebugFileInfo.h>

ClassifyProblemInstance* ClassifyProblemInstance::mClassifyProblemInstance = NULL;

ClassifyProblemInstance::ClassifyProblemInstance(const string& problem_db_file,
        string source_file) : mClassifierObject (NULL) {
    // TODO Auto-generated constructor stub
    create_classifier(problem_db_file, source_file);
}

void ClassifyProblemInstance::Clean() {
    if (mClassifyProblemInstance != NULL) {
        delete mClassifyProblemInstance;
    }

    Py_Finalize();
}

ClassifyProblemInstance::~ClassifyProblemInstance() {
    // TODO Auto-generated destructor stub
    Py_DECREF(mClassifierObject);
    mClassifierObject = NULL;
}

unsigned ClassifyProblemInstance::getEstimatorCount(string source_file) const {
    string db_file = omnetpp::getSimulation()->getSystemModule()->par(
                        "pathToTraceEstimatorFile").stringValue();
    //string trace_name = omnetpp::getSimulation()->getSystemModule()->par(
    //        "interArrivalsFile").stringValue();

    EstimatorReader estimator_reader = EstimatorReader(db_file);
    return estimator_reader.GetEstimatorCount(source_file);
}

void ClassifyProblemInstance::create_classifier(string problem_db_file, string source_file) {
    PyObject *module, *dict, *python_class;
    const char* classifierScript = "ETClassifier";

    char* pPath = getenv( "PYTHONPATH" );

    if (pPath!=NULL) {
        printf ("The PYTHONPATH path is: %s\n", pPath);
    } else {
        assert(false);
    }

    Py_Initialize();

    module = PyImport_ImportModule(classifierScript);
    if(module == NULL || PyErr_Occurred() != NULL) {
        PyErr_Print();
        assert(module != NULL);
    }
    //printf("1\n");
    fflush(stdout);

    // Deleting memory for object pName
    //Py_DECREF(module_name);
    //printf("2\n");
    fflush(stdout);

    //printf("3\n");
    fflush(stdout);
    assert(module != NULL);

    if (module != NULL) {
        fflush(stdout);
        //printf("Found\n");
        fflush(stdout);
        //printf("4\n");
        fflush(stdout);

        // dict is a borrowed reference.
        dict = PyModule_GetDict(module);
        //printf("5\n");
        fflush(stdout);

        if (dict == NULL) {
            PyErr_Print();
            std::cerr << "Fails to get the dictionary.\n";
            Py_DECREF(module);
            return;
        }

        //printf("6\n");
        fflush(stdout);
        Py_DECREF(module);

        // Builds the name of a callable class
        python_class = PyDict_GetItemString(dict, "ETClassifier");
        //printf("7\n");

        if (python_class == NULL) {
            PyErr_Print();
            std::cerr << "Fails to get the Python class.\n";
            Py_DECREF(dict);
            return;
        }

        Py_DECREF(dict);
        //printf("8\n");
        fflush(stdout);

        // Creates an instance of the class
        if (PyCallable_Check(python_class)) {
            //object = PyObject_CallFunction(python_class, "abc", "1");
            PyObject *args;
            bool comparePastWorkloadForSimilarity = omnetpp::getSimulation()->getSystemModule()->par(
                    "comparePastWorkloadForSimilarity").boolValue();
            //long int estimatorsCount = omnetpp::getSimulation()->getSystemModule()->par(
            //                    "estimatorsCount").longValue();

            long int estimatorsCount =  getEstimatorCount(source_file);
            args = Py_BuildValue("ssii", problem_db_file.c_str(), source_file.c_str(),
                    estimatorsCount, comparePastWorkloadForSimilarity);
            mClassifierObject = PyObject_CallObject(python_class, args);
            string file_name = DebugFileInfo::getInstance()->GetDebugFilePath();
                 //   "/home/ashutosp/Dropbox/regression/HP_triggers_arrival_rate";

            std::ofstream myfile;
            myfile.open(file_name, ios::app);
            myfile << "estimatorsCount = " << estimatorsCount << endl;
            myfile.close();

            string trace_estimator = "/home/ashutosp/Dropbox/regression/trace_estimator.csv";
            myfile.open(trace_estimator, std::ios::app);
            myfile << source_file << "," << estimatorsCount << std::endl;
            myfile.close();

            if(mClassifierObject == NULL || PyErr_Occurred() != NULL) {
                PyErr_Print();
                assert(mClassifierObject != NULL);
            }

            PyObject *success = PyObject_CallMethod(mClassifierObject, "train", NULL);

            if(success == NULL || PyErr_Occurred() != NULL) {
                PyErr_Print();
            }

            Py_DECREF(args);
            Py_DECREF(python_class);
        } else {
            std::cout << "Cannot instantiate the Python class" << std::endl;
            Py_DECREF(python_class);
            return;
        }
    }
}

double ClassifyProblemInstance::useReactive(HPModel* hpModel, const TimeSeries& arrivalRates) const {
    double useReactive = -1;
    // TODO pass as integers/floats rather than string
    string discretizedBrownoutFactor = to_string(1
                + (hpModel->getNumberOfBrownoutLevels() - 1)
                    * hpModel->getConfiguration().getBrownOutFactor());

    string serverCountA = to_string(hpModel->getConfiguration().getActiveServers(
            MTServerAdvance::ServerType::A));
    string serverCountB = to_string(hpModel->getConfiguration().getActiveServers(
            MTServerAdvance::ServerType::B));
    string serverCountC = to_string(hpModel->getConfiguration().getActiveServers(
            MTServerAdvance::ServerType::C));

    int addServerAState = 0;
    int addServerBState = 0;
    int addServerCState = 0;

    int addServerState = 0;

    if (hpModel->getConfiguration().getBootRemain() > 0) {
        int bootPeriods = hpModel->getBootDelay()
                / hpModel->getEvaluationPeriod();

        addServerState = min(
                bootPeriods - hpModel->getConfiguration().getBootRemain(),
                bootPeriods);

        MTServerAdvance::ServerType bootType =
                hpModel->getConfiguration().getBootType();

        switch (bootType) {
            case MTServerAdvance::ServerType::A:
                addServerAState = addServerState;
                break;
            case MTServerAdvance::ServerType::B:
                addServerBState = addServerState;
                break;
            case MTServerAdvance::ServerType::C:
                addServerCState = addServerState;
                break;
            case MTServerAdvance::ServerType::NONE:
                assert(false);
        }
    }

    // Write workload distribution
    string workloadA = to_string(hpModel->getConfiguration().getTraffic(MTServerAdvance::ServerType::A));
    string workloadB = to_string(hpModel->getConfiguration().getTraffic(MTServerAdvance::ServerType::B));
    string workloadC = to_string(hpModel->getConfiguration().getTraffic(MTServerAdvance::ServerType::C));

    //double avg_response_time = hpModel->getCurrentResponseTime();
    double avg_response_time = hpModel->getObservations().avgResponseTime;

    PyObject *value = PyObject_CallMethod(mClassifierObject, "classifier_predict",
            "(sssssssssssssssssssssssssssssss)", discretizedBrownoutFactor.c_str(), serverCountA.c_str(),
            serverCountB.c_str(), serverCountC.c_str(), to_string(addServerAState).c_str(),
            to_string(addServerBState).c_str(), to_string(addServerCState).c_str(),
            workloadA.c_str(), workloadB.c_str(), workloadC.c_str(), to_string(avg_response_time).c_str(),
            to_string(arrivalRates[0]).c_str(), to_string(arrivalRates[1]).c_str(),
            to_string(arrivalRates[2]).c_str(), to_string(arrivalRates[3]).c_str(),
            to_string(arrivalRates[4]).c_str(), to_string(arrivalRates[5]).c_str(),
            to_string(arrivalRates[6]).c_str(), to_string(arrivalRates[7]).c_str(),
            to_string(arrivalRates[8]).c_str(), to_string(arrivalRates[9]).c_str(),
            to_string(arrivalRates[10]).c_str(), to_string(arrivalRates[11]).c_str(),
            to_string(arrivalRates[12]).c_str(), to_string(arrivalRates[13]).c_str(),
            to_string(arrivalRates[14]).c_str(), to_string(arrivalRates[15]).c_str(),
            to_string(arrivalRates[16]).c_str(), to_string(arrivalRates[17]).c_str(),
            to_string(arrivalRates[18]).c_str(), to_string(arrivalRates[19]).c_str());


    if (value == NULL) {
        PyErr_Print();
        assert(false);
    } else {
        printf("Result of call: %f\n", PyFloat_AsDouble(value));
        //useReactive = (PyFloat_AsDouble(value) == 1.0) ? true : false;
        useReactive = PyFloat_AsDouble(value);
        Py_DECREF(value);
    }

    string file_name = DebugFileInfo::getInstance()->GetDebugFilePath();
       //     "/home/ashutosp/Dropbox/regression/HP_triggers_arrival_rate";
    std::ofstream myfile;

    myfile.open(file_name, ios::app);
    myfile << "Features = [" << discretizedBrownoutFactor << ", " << serverCountA << ", "
            << serverCountB << ", " << serverCountC << ", " << addServerAState << ", "
            << addServerBState << ", " << addServerCState << ", " << workloadA
            << ", " << workloadB << ", " << workloadC << ", " << avg_response_time << ", "
            << arrivalRates[0] << ", " << arrivalRates[1] << ", " << arrivalRates[2] << ", "
            << arrivalRates[3] << ", " << arrivalRates[4] << ", " << arrivalRates[5] << ", "
            << arrivalRates[6] << ", " << arrivalRates[7] << ", " << arrivalRates[8] << ", "
            << arrivalRates[9] << ", " << arrivalRates[10] << ", " << arrivalRates[11] << ", "
            << arrivalRates[12] << ", " << arrivalRates[13] << ", " << arrivalRates[14] << ", "
            << arrivalRates[15] << ", " << arrivalRates[16] << ", " << arrivalRates[17] << ", "
            << arrivalRates[18] << ", " << arrivalRates[19] << "]\n";
            myfile << "Prediction = " << PyFloat_AsDouble(value) << "\n";
    myfile.close();

    return useReactive;
}
