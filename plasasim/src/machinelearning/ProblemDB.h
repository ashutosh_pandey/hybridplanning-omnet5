//===============================================================================
// Name        : ProblemDB.h
// Author      : Ashutosh Pandey
// Version     :
// Copyright   : Your copyright notice
// Description : Code to read and store sample problem database from a csv file
// ===============================================================================

#ifndef PROBLEMDB_PROBLEMDB_H_
#define PROBLEMDB_PROBLEMDB_H_

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <omnetpp.h>
#include <HPLoadBalancer.h>

using namespace std;

class ProblemDB {
public:
	typedef vector<double> TimeSeries;

	struct ProblemData {
		string m_sample_problem_dir;
		float m_dimmer;
		unsigned m_server_A_count;
		unsigned m_server_B_count;
		unsigned m_server_C_count;
		unsigned m_server_A_status;
		unsigned m_server_B_status;
		unsigned m_server_C_status;
		unsigned m_server_A_load;
        unsigned m_server_B_load;
        unsigned m_server_C_load;
		TimeSeries m_work_load;
		unsigned m_use_reactive;
	};

	typedef vector<string> Strings;

	static ProblemDB* getInstance();

	TimeSeries get_entire_time_series(const string& problem_dir) const;
	const ProblemData* get_problem_data(const string& problem_dir) const;
	void get_sample_problem_dirs(Strings& problems) const;
	const double* get_time_series_for_comparision(const string& problem_dir) const;
	bool populate_db();
	static void clean_db();
    ~ProblemDB();


private:
	static ProblemDB* m_instance;
	unsigned m_horizon;
	unsigned m_warmup_period;


	typedef map<string, ProblemData*> Database;
	typedef map<string, double*>TimeSeriesData;
	Database m_sample_problem_db;
	TimeSeriesData m_time_series_data;

	ProblemDB();
};


#endif /* PROBLEMDB_PROBLEMDB_H_ */
