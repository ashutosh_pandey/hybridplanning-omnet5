/*
 * EstimatorReader.cc
 *
 *  Created on: Jul 18, 2018
 *      Author: ashutosp
 */

#include <EstimatorReader.h>
#include <boost/tokenizer.hpp>
#include <assert.h>
#include <stdio.h>
#include <iostream>

EstimatorReader::EstimatorReader(const string& input_file) : m_input_file(input_file) {
    // TODO Auto-generated constructor stub
    ReadFile();
}

EstimatorReader::~EstimatorReader() {
    // TODO Auto-generated destructor stub
    m_estimator_db.clear();
}

void EstimatorReader::ReadFile() {
    ifstream problem_db_file (m_input_file.c_str(), std::ifstream::in);
    string line;

    unsigned line_count = 0;
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

    assert(problem_db_file.good());

    while ( problem_db_file.good() ) {
        getline ( problem_db_file, line);

        //cout << line << endl;

        if (line == "") {
            //  Hack
            continue;
        }

        ++line_count;

        tokenizer tokens(line, boost::char_separator<char>(",\n"));
        tokenizer::iterator it = tokens.begin();

        string trace = "";
        unsigned estimator_count = 0;

        if (it != tokens.end()) {
            trace = *it;
            ++it;
        } else {
            assert(false);
        }

        if (it != tokens.end()) {
            estimator_count = strtoul((*it).c_str(), nullptr, 0);
            ++it;
        } else {
            assert(false);
        }

        m_estimator_db[trace] = estimator_count;
    }
}

unsigned EstimatorReader::GetEstimatorCount(string trace_name) {
    assert(m_estimator_db.find(trace_name) != m_estimator_db.end());
    return m_estimator_db[trace_name];
}

