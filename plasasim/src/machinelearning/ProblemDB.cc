//===============================================================================
// Name        : ProblemDB.cpp
// Author      : Ashutosh Pandey
// Version     :
// Copyright   : Your copyright notice
// Description : Code to read and store sample problem database from a csv file
// ===============================================================================

#include <ProblemDB.h>
#include <boost/tokenizer.hpp>
#include <fstream>
#include <string>
#include <HPModel.h>

/* Null, because instance will be initialized on demand. */
ProblemDB* ProblemDB::m_instance = NULL;

ProblemDB* ProblemDB::getInstance() {
    if (m_instance == NULL) {
        m_instance = new ProblemDB();
    }

    return m_instance;
}

ProblemDB::ProblemDB() :m_horizon (0) {
    // Code duplicated from HPModel::initialize
    int maxServersA = omnetpp::getSimulation()->getSystemModule()->par("maxServersA");
    int maxServersB = omnetpp::getSimulation()->getSystemModule()->par("maxServersB");
    int maxServersC = omnetpp::getSimulation()->getSystemModule()->par("maxServersC");

    int maxServers = maxServersA + maxServersB + maxServersC;

    double evaluationPeriod =
            omnetpp::getSimulation()->getSystemModule()->par("evaluationPeriod").doubleValue();
    double bootDelay = omnetpp::getSimulation()->getSystemModule()->par("bootDelay");
    m_horizon = max(5.0, ceil(bootDelay / evaluationPeriod) * (maxServers - 1) + 1);
    m_warmup_period = max(10, int(ceil(omnetpp::getSimulation()->getWarmupPeriod().dbl() / evaluationPeriod)));
}

ProblemDB::~ProblemDB() {
    Database::iterator it = m_sample_problem_db.begin();

    while(it != m_sample_problem_db.end()) {
        //cout << it->first << endl;
        if (it->second != NULL) {
            delete it->second;
        }
        ++it;
    }

    m_sample_problem_db.clear();

    TimeSeriesData::iterator itr = m_time_series_data.begin();
    while(itr != m_time_series_data.end()) {
        //cout << itr->first << endl;
        if (itr->second != NULL) {
            delete itr->second;
        }
        ++itr;
    }

    m_time_series_data.clear();
}

// TODO This function is not being called anywhere
void ProblemDB::clean_db() {
    delete m_instance;
    m_instance = NULL;
}

bool ProblemDB::populate_db() {
	bool success = false;

	string profiled_problem_file = omnetpp::getSimulation()->getSystemModule()->par(
            "pathToProfiledProblems").stdstringValue();

	ifstream problem_db_file (profiled_problem_file.c_str(), std::ifstream::in);
	string line;

	unsigned line_count = 0;
	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

	assert(problem_db_file.good());

	while ( problem_db_file.good() ) {
		getline ( problem_db_file, line);

		string sample_problem_dir;
		ProblemData* data = new ProblemData();

	    cout << line << endl;

	    if (line == "") {
	    	//  Hack
	    	continue;
	    }

		++line_count;

		tokenizer tokens(line, boost::char_separator<char>(",\n"));
	    tokenizer::iterator it = tokens.begin();

	    if (it != tokens.end()) {
	    	sample_problem_dir = *it;
	    	cout << "sample_problem_dir = " << sample_problem_dir << endl;
	        ++it;
	    } else {
	    	assert(false);
	    }

	    if (it != tokens.end()) {
	    	data->m_dimmer = atof((*it).c_str());
	    	cout << "m_dimmer = " << data->m_dimmer << endl;
	    	++it;
	    } else {
	       	assert(false);
	    }

	    if (it != tokens.end()) {
	    	data->m_server_A_count = strtoul ((*it).c_str(), nullptr, 0);
	    	cout << "m_server_A_count = " << data->m_server_A_count << endl;
	    	++it;
	    } else {
	       	assert(false);
	    }

	    if (it != tokens.end()) {
	    	data->m_server_B_count = strtoul ((*it).c_str(), nullptr, 0);
	    	cout << "m_server_B_count = " << data->m_server_B_count << endl;
	    	++it;
	    } else {
	       	assert(false);
	    }

	    if (it != tokens.end()) {
	    	data->m_server_C_count = strtoul ((*it).c_str(), nullptr, 0);
	    	cout << "m_server_C_count = " << data->m_server_C_count << endl;
	    	++it;
	    } else {
	       	assert(false);
	    }

	    if (it != tokens.end()) {
	    	data->m_server_A_status = strtoul ((*it).c_str(), nullptr, 0);
	    	cout << "m_server_A_status = " << data->m_server_A_status << endl;
	    	++it;
	    } else {
	       	assert(false);
	    }

	    if (it != tokens.end()) {
	    	data->m_server_B_status = strtoul ((*it).c_str(), nullptr, 0);
	    	cout << "m_server_B_status = " << data->m_server_B_status << endl;
	    	++it;
	    } else {
	       	assert(false);
	    }

	    if (it != tokens.end()) {
	    	data->m_server_C_status = strtoul ((*it).c_str(), nullptr, 0);
	    	cout << "m_server_C_status = " << data->m_server_C_status << endl;
	    	++it;
	    } else {
	       	assert(false);
	    }

	    if (it != tokens.end()) {
	        data->m_server_A_load = strtoul ((*it).c_str(), nullptr, 0);
	        cout << "m_server_A_load = " << data->m_server_A_load << endl;
	        ++it;
	    } else {
	        assert(false);
	    }

	    if (it != tokens.end()) {
	        data->m_server_B_load = strtoul ((*it).c_str(), nullptr, 0);
	        cout << "m_server_B_load = " << data->m_server_B_load << endl;
	        ++it;
	    } else {
	        assert(false);
	    }

	    if (it != tokens.end()) {
	        data->m_server_C_load = strtoul ((*it).c_str(), nullptr, 0);
	        cout << "m_server_C_load = " << data->m_server_C_load << endl;
	        ++it;
	    } else {
	        assert(false);
	    }

	    if (it != tokens.end()) {
	        unsigned index = 0;
	        cout << "m_warmup_period = " << m_warmup_period << " m_horizon = " << m_horizon << endl;

	        // plus 1 because predictions has horizon + 1 entries
	    	while (index < m_warmup_period + m_horizon + 1) {
		       	cout << "index = " << index << " workload = " << *it << endl;
	    		(data->m_work_load).push_back(atof((*it).c_str()));
	        	++it;
	        	++index;
	    	}

	    	assert(index == (data->m_work_load).size());
	    } else {
	    	assert(false);
	    }

        if (it != tokens.end()) {
            data->m_use_reactive = strtoul ((*it).c_str(), nullptr, 0);
            cout << "m_use_reactive = " << data->m_use_reactive << endl;
            ++it;
        } else {
            assert(false);
        }

        assert(it == tokens.end());

	    m_sample_problem_db.insert(std::make_pair(sample_problem_dir, data));
	}

	// Some optimization to make time-series similarity fast.
    // Store only the values to be compared
	unsigned time_series_length_for_comparision = (omnetpp::getSimulation()->getSystemModule()->par("comparePastWorkloadForSimilarity").boolValue()) ?
	            m_horizon + m_warmup_period + 1 : m_horizon + 1;

	Database::const_iterator it = m_sample_problem_db.begin();

	while (it != m_sample_problem_db.end()) {
	    double* time_series = new double(time_series_length_for_comparision);
	    TimeSeries::const_iterator itr = it->second->m_work_load.begin();
	    unsigned index = 0;


	    if (!omnetpp::getSimulation()->getSystemModule()->par("comparePastWorkloadForSimilarity").boolValue()) {
	        while (index < m_warmup_period) {
	            ++itr;
	            ++index;
	        }
	    }

	    unsigned i = 0;

	    while (itr != it->second->m_work_load.end()) {
	        time_series[i++] = *itr;
	        ++itr;
	    }

	    assert(index + i == m_horizon + m_warmup_period + 1);
	    assert(index + i == it->second->m_work_load.size());

	    m_time_series_data.insert(make_pair(it->first, time_series));
	    ++it;
	}

	cout << "Read line_count = " << line_count << endl;

	problem_db_file.close();

	return success;
}

void ProblemDB::get_sample_problem_dirs(Strings& problems) const {
	Database::const_iterator it = m_sample_problem_db.begin();

	while (it != m_sample_problem_db.end()) {
		problems.push_back(it->first);
		++it;
	}
}

ProblemDB::TimeSeries ProblemDB::get_entire_time_series(const string& problem_dir) const {
	/*cout << problem_dir << endl;
	Database::const_iterator it = m_sample_problem_db.begin();

	while (it != m_sample_problem_db.end()) {
		cout << it->first << endl;
		//cout << it->second->m_work_load << endl;

		if (it->first == problem_dir) {
			cout << "Match found" << endl;
		}

		++it;
	}*/

	Database::const_iterator itr = m_sample_problem_db.find(problem_dir);
	assert(itr != m_sample_problem_db.end());

	return itr->second->m_work_load;
}

const double* ProblemDB::get_time_series_for_comparision(const string& problem_dir) const {
    TimeSeriesData::const_iterator itr = m_time_series_data.find(problem_dir);
    assert(itr != m_time_series_data.end());

    return itr->second;
}

const ProblemDB::ProblemData* ProblemDB::get_problem_data(const string& problem_dir) const {
	Database::const_iterator itr = m_sample_problem_db.find(problem_dir);
	assert(itr != m_sample_problem_db.end());

	return itr->second;
}


