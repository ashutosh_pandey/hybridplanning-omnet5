//
 // Copyright (c) 2015 Carnegie Mellon University. All Rights Reserved.

 // Redistribution and use in source and binary forms, with or without
 // modification, are permitted provided that the following conditions
 // are met:

 // 1. Redistributions of source code must retain the above copyright
 // notice, this list of conditions and the following acknowledgments
 // and disclaimers.

 // 2. Redistributions in binary form must reproduce the above
 // copyright notice, this list of conditions and the following
 // disclaimer in the documentation and/or other materials provided
 // with the distribution.

 // 3. The names "Carnegie Mellon University," "SEI" and/or "Software
 // Engineering Institute" shall not be used to endorse or promote
 // products derived from this software without prior written
 // permission. For written permission, please contact
 // permission@sei.cmu.edu.

 // 4. Products derived from this software may not be called "SEI" nor
 // may "SEI" appear in their names without prior written permission of
 // permission@sei.cmu.edu.

 // 5. Redistributions of any form whatsoever must retain the following
 // acknowledgment:

 // This material is based upon work funded and supported by the
 // Department of Defense under Contract No. FA8721-05-C-0003 with
 // Carnegie Mellon University for the operation of the Software
 // Engineering Institute, a federally funded research and development
 // center.

 // Any opinions, findings and conclusions or recommendations expressed
 // in this material are those of the author(s) and do not necessarily
 // reflect the views of the United States Department of Defense.

 // NO WARRANTY. THIS CARNEGIE MELLON UNIVERSITY AND SOFTWARE
 // ENGINEERING INSTITUTE MATERIAL IS FURNISHED ON AN "AS-IS"
 // BASIS. CARNEGIE MELLON UNIVERSITY MAKES NO WARRANTIES OF ANY KIND,
 // EITHER EXPRESSED OR IMPLIED, AS TO ANY MATTER INCLUDING, BUT NOT
 // LIMITED TO, WARRANTY OF FITNESS FOR PURPOSE OR MERCHANTABILITY,
 // EXCLUSIVITY, OR RESULTS OBTAINED FROM USE OF THE MATERIAL. CARNEGIE
 // MELLON UNIVERSITY DOES NOT MAKE ANY WARRANTY OF ANY KIND WITH
 // RESPECT TO FREEDOM FROM PATENT, TRADEMARK, OR COPYRIGHT
 // INFRINGEMENT.

 // This material has been approved for public release and unlimited
 // distribution.

 // DM-0002494
 //
/*
 * MGCQueue.cpp
 *
 *  Created on: Oct 20, 2015
 *      Author: ashutosp
 */

#include <MGCQueue.h>
#include "MTServerAdvance.h"
#include "HPConfiguration.h"

MGCQueue::MGCQueue() {
    // TODO Auto-generated constructor stub

}

MGCQueue::~MGCQueue() {
    // TODO Auto-generated destructor stub
}

double MGCQueue::getResponseTime(const HPModel * model, double arrivalRate) {
    //const HPModel::ServerInfo* serverInfoA = model->getServerInfoObj(MTServerAdvance::ServerType::A);
    //const HPModel::ServerInfo* serverInfoB = model->getServerInfoObj(MTServerAdvance::ServerType::B);
    //const HPModel::ServerInfo* serverInfoC = model->getServerInfoObj(MTServerAdvance::ServerType::C);

    HPModel* hpModel = const_cast<HPModel*>(model);

    const HPConfiguration configuration = hpModel->getConfiguration();
    int serverCountA = configuration.getActiveServers(MTServerAdvance::ServerType::A);
    int serverCountB = configuration.getActiveServers(MTServerAdvance::ServerType::B);
    int serverCountC = configuration.getActiveServers(MTServerAdvance::ServerType::C);

    double a = ((double)configuration.getTraffic(MTServerAdvance::ServerType::A));
    double b = ((double)configuration.getTraffic(MTServerAdvance::ServerType::B));
    double c = ((double)configuration.getTraffic(MTServerAdvance::ServerType::C));

    double d = a * 25/100;
    double e = b * 25/100;
    double f = c * 25/100;

    double trafficA = ((double)configuration.getTraffic(MTServerAdvance::ServerType::A))* 25/100;
    double trafficB = ((double)configuration.getTraffic(MTServerAdvance::ServerType::B))* 25/100;
    double trafficC = ((double)configuration.getTraffic(MTServerAdvance::ServerType::C))* 25/100;

    double brownoutFactor =  configuration.getBrownOutFactor();

    double MAX_ARRIVALA_CAPACITY = omnetpp::getSimulation()->getSystemModule()->par("max_arrival_capacity_A").doubleValue();
    double MAX_ARRIVALA_CAPACITY_LOW = omnetpp::getSimulation()->getSystemModule()->par("max_arrival_capacity_low_A").doubleValue();
    double MAX_ARRIVALB_CAPACITY = omnetpp::getSimulation()->getSystemModule()->par("max_arrival_capacity_B").doubleValue();
    double MAX_ARRIVALB_CAPACITY_LOW = omnetpp::getSimulation()->getSystemModule()->par("max_arrival_capacity_low_B").doubleValue();
    double MAX_ARRIVALC_CAPACITY = omnetpp::getSimulation()->getSystemModule()->par("max_arrival_capacity_C").doubleValue();
    double MAX_ARRIVALC_CAPACITY_LOW = omnetpp::getSimulation()->getSystemModule()->par("max_arrival_capacity_low_C").doubleValue();

    double serviceRateA = brownoutFactor * (MAX_ARRIVALA_CAPACITY_LOW) + (1 - brownoutFactor) * MAX_ARRIVALA_CAPACITY;
    double serviceRateB = brownoutFactor * (MAX_ARRIVALB_CAPACITY_LOW) + (1 - brownoutFactor) * MAX_ARRIVALB_CAPACITY;
    double serviceRateC = brownoutFactor * (MAX_ARRIVALC_CAPACITY_LOW) + (1 - brownoutFactor) * MAX_ARRIVALC_CAPACITY;

    //double interArrivalScaleFactorForDecision = simulation.getSystemModule()
    //        ->getParentModule()->getSubmodule("adaptationManager")->par("interArrivalScaleFactorForDecision");
    //double interarrivalMean = arrivalRate * 0.1 * (serverCountA + serverCountB + serverCountC);
    double interarrivalMean = arrivalRate;

    double rhoA = trafficA/(serviceRateA * interarrivalMean);
    double rhoB = trafficB/(serviceRateB * interarrivalMean);
    double rhoC = trafficC/(serviceRateC * interarrivalMean);

    double throughput = 1/interarrivalMean;

    double rt_A = 1/(serviceRateA - (throughput * trafficA));
    double rt_B = 1/(serviceRateB - (throughput * trafficB));
    double rt_C = 1/(serviceRateC - (throughput * trafficC));

    double expected_wait_time = trafficA * rt_A + trafficB * rt_B + trafficC * rt_C ;
    double response_time = (interarrivalMean = 0 ? 0 : ((rhoA > 1) | (rhoB > 1) | (rhoC > 1)) ? DBL_MAX : expected_wait_time);

    std::cout << "MGCQueue::getResponseTime = " << response_time << std::endl;

    if (response_time > 1) {
        std::cout << "MGCQueue::getResponseTime Hello" << std::endl;
    }

    return response_time;
}


