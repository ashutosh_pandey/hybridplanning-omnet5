/*
 * TimeSeriesSimilarity.cc
 *
 *  Created on: Jan 9, 2018
 *      Author: ashutosp
 */

#include <TimeSeriesSimilarity.h>
#include <ctime>
#include <Python.h>

TimeSeriesSimilarity* TimeSeriesSimilarity::mTimeSeriesSimilarity = NULL;

TimeSeriesSimilarity::TimeSeriesSimilarity() {
    // TODO Auto-generated constructor stub

}

TimeSeriesSimilarity::~TimeSeriesSimilarity() {
    // TODO Auto-generated destructor stub
}

double TimeSeriesSimilarity::GetSimilarity() {
   // printf("Embedding Python\n");
    PyObject *pName = NULL;
    PyObject *pModule = NULL;
    PyObject *pFunc = NULL;
    PyObject *pArgs = NULL;
    PyObject *pValue = NULL;
    PyObject *vec = NULL;
    PyObject *vec1 = NULL;
    //PyObject *pName, *pModule, *pFunc;
    //PyObject *pArgs, *pValue, *vec, *vec1;

    char* pPath;
    pPath = getenv ("PYTHONPATH");

    if (pPath!=NULL) {
        printf ("The PYTHONPATH path is: %s",pPath);
    } else {
        assert(false);
    }

    const char* script = "TimeSeriesSimilarity";
    const char* function = "TimeSeriesSimilarity";

    Py_Initialize();
    pName = PyUnicode_FromString(script);
    //pName = PyString_FromString(file);
    pModule = PyImport_Import(pName);
    //pName = PyString_FromString("fastdtw");
    //pModule = PyImport_Import(pName);
    import_array();

    // Deleting memory for object pName
    Py_DECREF(pName);

    if (pModule != NULL) {
        //printf("Step-1\n");
        pFunc = PyObject_GetAttrString(pModule, function);
        //fflush( stdout );
        //printf("Step-2\n");

        if (pFunc && PyCallable_Check(pFunc)) {
            // define a vector
            double* v = new double[5];
            v[0] = 1.0;
            v[1] = 2.0;
            v[2] = 3.0;
            v[3] = 4.0;
            v[4] = 5.0;

            double* v1 = new double[5];
            v1[0] = 1.0;
            v1[1] = 2.0;
            v1[2] = 3.0;
            v1[3] = 4.0;
            v1[4] = 5.0;

            //fflush( stdout );
            //printf("Step-3\n");

            npy_intp vdim[] = { 5 };
            //npy_intp dims[1];
            //dims[0] = 3;

            //fflush( stdout );
           // printf("Step-4\n");

            vec = PyArray_SimpleNewFromData(1, vdim, (int)PyArray_DOUBLE, (void*)v);
            vec1 = PyArray_SimpleNewFromData(1, vdim, (int)PyArray_DOUBLE, (void*)v1);

            //fflush( stdout );
            //printf("Step-5\n");

            pArgs = PyTuple_New(2);
            PyTuple_SetItem(pArgs, 0, vec);
            PyTuple_SetItem(pArgs, 1, vec1);

            //int start_s=clock();
            // the code you wish to time goes here


            //for (int i =0; i < 1000; ++i) {
            pValue = PyObject_CallObject(pFunc, pArgs);
            //}
            //int stop_s=clock();
            //cout << "time: " << (stop_s-start_s)/double(CLOCKS_PER_SEC)*1000 << " milliseconds" << endl;

            Py_DECREF(pArgs);
            Py_DECREF(pName);
            if (pValue != NULL) {
                //printf("Result of call: %f\n", PyFloat_AsDouble(pValue));
                Py_DECREF(pValue);
            }
            delete [] v;
            delete [] v1;
        } else {
            if (PyErr_Occurred()) {
                PyErr_Print();
                fprintf(stderr, "Cannot find function \"%s\"\n", function);
            }
        }
        Py_DECREF(pModule);
    } else {
        PyErr_Print();
        //fprintf(stderr, "Failed to load \"%s\"\n", argv[1]);
        return 1;
    }

    Py_Finalize();
    return 0;

}

