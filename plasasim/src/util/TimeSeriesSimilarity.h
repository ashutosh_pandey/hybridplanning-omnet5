/*
 * TimeSeriesSimilarity.h
 *
 *  Created on: Jan 9, 2018
 *      Author: ashutosp
 */

#ifndef UTIL_TIMESERIESSIMILARITY_H_
#define UTIL_TIMESERIESSIMILARITY_H_

#include <stdio.h>
#include <iostream>
#include "numpy/arrayobject.h"

using namespace std;

class TimeSeriesSimilarity {
    static TimeSeriesSimilarity* mTimeSeriesSimilarity;
    TimeSeriesSimilarity();

public:

    static TimeSeriesSimilarity* getInstance() {
        if (mTimeSeriesSimilarity == NULL) {
            mTimeSeriesSimilarity = new TimeSeriesSimilarity();
        }

        return mTimeSeriesSimilarity;
    }

    virtual ~TimeSeriesSimilarity();

    double GetSimilarity();
};

#endif /* UTIL_TIMESERIESSIMILARITY_H_ */
