mdp
// Init must include 
// const double addServer_LATENCY 
// const int HORIZON
// const double PERIOD
// const int BROWNOUT_LEVELS
// const int MAX_SERVERS
// const int ini_servers
// const int ini_addServer_state
// const int ini_removeServer_state
// const int ini_brownout
// const double serviceTimeMean
// const double serviceTimeVariance
// const double lowServiceTimeMean
// const double lowServiceTimeVariance
// const int threads
///
//#init
label "initState" = servers = ini_servers & addServer_state = ini_addServer_state
	& brownout = ini_brownout;

label "final" = time = HORIZON + 1;
formula env_go = env_turn & time < HORIZON + 1;
formula sys_turn = !env_turn;

module env
	time : [0..HORIZON + 1] init 0;
	env_turn : bool init false;
//#environment

// tactic concurrency rules
formula addServer_compatible = !removeServer_used;
formula removeServer_compatible = !addServer_used;
formula increaseBrownout_compatible = !decreaseBrownout_used;
formula decreaseBrownout_compatible = !increaseBrownout_used;

// tactic
formula addServer_used = addServer_state != 0;
const int addServer_LATENCY_PERIODS = ceil(addServer_LATENCY / PERIOD);
module addServer
	addServer_state : [0..addServer_LATENCY_PERIODS] init ini_addServer_state;
	addServer_go : bool init true;


	[addServer_start] sys_turn & addServer_go // can go
		& addServer_state = 0 // tactic has not been started
		& servers < MAX_SERVERS & addServer_compatible // applicability conditions
		-> (addServer_state' = 1) & (addServer_go' = false);

	// tactic applicable but not used
	[] sys_turn & addServer_go // can go
		& addServer_state = 0  & addServer_compatible // tactic has not been started
		& servers < MAX_SERVERS // applicability conditions
		-> (addServer_go' = false);


	// progress of the tactic
	[] sys_turn & addServer_go & addServer_state > 0 & addServer_state < addServer_LATENCY_PERIODS
		-> 1 : (addServer_state' = addServer_state + 1) & (addServer_go' = false);
	[addServer_complete] sys_turn & addServer_go & addServer_state = addServer_LATENCY_PERIODS
		-> 1 : (addServer_state' = 0) & (addServer_go' = true); // so that it can start again at this time if needed

	// pass if the tactic is not applicable
	[] sys_turn & addServer_go & addServer_state = 0
		& !(servers < MAX_SERVERS & addServer_compatible ) // applicability conditions negated
		-> 1 : (addServer_go' = false);


	[clk] !addServer_go -> 1 : (addServer_go' = true);
endmodule


// tactic
module removeServer
	removeServer_go : bool init true;
	removeServer_used : bool init false;

	[removeServer_start] sys_turn & removeServer_go
		& servers > 1 & removeServer_compatible // applicability conditions
		-> (removeServer_go' = false) & (removeServer_used' = true);

	// tactic applicable but not used
	[] sys_turn & removeServer_go // can go
		& servers > 1 & removeServer_compatible // applicability conditions
		-> (removeServer_go' = false);

	// pass if the tactic is not applicable
	[] sys_turn & removeServer_go 
		& !(servers > 1 & removeServer_compatible) // applicability conditions negated
		-> 1 : (removeServer_go' = false);

	[clk] !removeServer_go -> 1 : (removeServer_go' = true) & (removeServer_used' = false);
endmodule


// tactic
module increaseBrownout
	increaseBrownout_go : bool init true;
	increaseBrownout_used : bool init false;

	[increaseBrownout_start] sys_turn & increaseBrownout_go
		& brownout < BROWNOUT_LEVELS & increaseBrownout_compatible // applicability conditions
		-> (increaseBrownout_go' = false) & (increaseBrownout_used' = true);

	// tactic applicable but not used
	[] sys_turn & increaseBrownout_go // can go
		& brownout < BROWNOUT_LEVELS & increaseBrownout_compatible // applicability conditions
		-> (increaseBrownout_go' = false);

	// pass if the tactic is not applicable
	[] sys_turn & increaseBrownout_go 
		& !(brownout < BROWNOUT_LEVELS & increaseBrownout_compatible) // applicability conditions negated
		-> 1 : (increaseBrownout_go' = false);

	[clk] !increaseBrownout_go -> 1 : (increaseBrownout_go' = true) & (increaseBrownout_used' = false);
endmodule

// tactic
module decreaseBrownout
	decreaseBrownout_go : bool init true;
	decreaseBrownout_used : bool init false;

	[decreaseBrownout_start] sys_turn & decreaseBrownout_go
		& brownout > 1 & decreaseBrownout_compatible // applicability conditions
		-> (decreaseBrownout_go' = false) & (decreaseBrownout_used' = true);

	// tactic applicable but not used
	[] sys_turn & decreaseBrownout_go // can go
		& brownout > 1 & decreaseBrownout_compatible // applicability conditions
		-> (decreaseBrownout_go' = false);

	// pass if the tactic is not applicable
	[] sys_turn & decreaseBrownout_go 
		& !(brownout > 1 & decreaseBrownout_compatible) // applicability conditions negated
		-> 1 : (decreaseBrownout_go' = false);

	[clk] !decreaseBrownout_go -> 1 : (decreaseBrownout_go' = true) & (decreaseBrownout_used' = false);
endmodule

// system
module sys
	servers : [1..MAX_SERVERS] init ini_servers;
	brownout : [1..BROWNOUT_LEVELS] init ini_brownout;
	
	[addServer_complete] servers < MAX_SERVERS -> 1 : (servers' = servers + 1);

	// for no-latency tactics sync on start
	[removeServer_start] servers > 1 -> 1 : (servers' = servers - 1);
	[increaseBrownout_start] brownout < BROWNOUT_LEVELS -> 1 : (brownout' = brownout + 1);
	[decreaseBrownout_start] brownout > 1 -> 1 : (brownout' = brownout - 1);
endmodule


// continuous equivalent for the brownout level
formula brownoutFactor = (brownout - 1) / (BROWNOUT_LEVELS - 1);
const double DBL_MAX = 1.5e+300;

//*****************************************************************
// Queuing model G/G/c LPS with round-robin allocation to servers
//*****************************************************************
formula interarrivalMean = stateValue;

// TODO for now assume arrivals have exponential distribution,
// otherwise we need to introduce variance into the probability tree
formula interArrivalVariance = pow(interarrivalMean, 2);

formula lambda = 1 / (interarrivalMean * servers);
formula beta = brownoutFactor * lowServiceTimeMean + (1-brownoutFactor) * serviceTimeMean;
formula rho = lambda * beta;
formula ca2 = interArrivalVariance * servers / pow(interarrivalMean * servers, 2);
formula cs2 = (brownoutFactor * lowServiceTimeVariance + (1-brownoutFactor) * serviceTimeVariance) / pow(beta, 2);
formula dp = pow(rho, threads * (1+cs2)/(ca2 + cs2));
formula rb = ((ca2 + cs2) / 2) * dp * beta / (1 - rho);
formula rz = ((ca2 + cs2) / (1 + cs2)) * (1 - dp) * beta / (1 - rho);
formula rt= (interarrivalMean=0 ? 0:(rho > 1 ? DBL_MAX : (rb + rz)));

// utility calculation
const double WEIGHT_COST = 0.1;
const double WEIGHT_RESPONSE_TIME = 0.5;
const double WEIGHT_CONTENT = 0.4;


// Response time to clients utility function
const double RT_THRESHOLD = 4.0;
formula uR = (rt>=0 & rt<=0.100? 1:0)
 	    +(rt>0.100 & rt<=0.200?    1+(0.99-1)*((rt-0.100)/(0.200-0.100))         :0)
	    +(rt>0.200 & rt<=0.500?    0.99+(0.90-0.99)*((rt-0.200)/(0.500-0.200))   :0)
	    +(rt>0.500 & rt<=1.000?   0.90+(0.75-0.90)*((rt-0.500)/(1.000-0.500))  :0)
	    +(rt>1.000 & rt<=1.500?  0.75+(0.5-0.75)*((rt-1.000)/(1.500-1.000)) :0)
	    +(rt>1.500 & rt<=2.000?  0.5+(0.25-0.5)*((rt-1.500)/(2.000-1.500))  :0)
	    +(rt>2.000 & rt<=RT_THRESHOLD?  0.25+(0-0.25)*((rt-2.000)/(RT_THRESHOLD-2.000))   :0)
	    +(rt>RT_THRESHOLD ? 0:0);


formula cost = (addServer_state > 0 ? servers + 1 : servers);

// Cost utility function
formula uC = (cost = 0 & cost = 1 ? 1 : 0)
	    +(cost = 2 ? 0.9 : 0)
	    +(cost = 3 ? 0.75 : 0)
	    +(cost >= 4 ? 0.5 : 0);

// Brownout utility function
formula uB = 1 - brownoutFactor;

//formula uTotal = (rt>=DBL_MAX? -1 : (uR=0? 0 : (WEIGHT_RESPONSE_TIME*uR + WEIGHT_COST*uC + WEIGHT_CONTENT*uB)));
formula uTotal = (rt>RT_THRESHOLD? ((cost < MAX_SERVERS | brownoutFactor < BROWNOUT_LEVELS) ?  -2 : -1) : (uR=0? 0 : (WEIGHT_RESPONSE_TIME*uR + WEIGHT_COST*uC + WEIGHT_CONTENT*uB)));

rewards "util"
	[clk] true : (10000.0 / (HORIZON + 1)) + (PERIOD)*(uTotal);
endrewards

